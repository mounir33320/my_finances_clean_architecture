<?php

namespace App\Tests\Integration\Infrastructure\Doctrine\Repository;

use App\Domain\User\Repository\UserRepositoryInterface;
use App\Infrastructure\Doctrine\Repository\Doctrine\UserDoctrineRepository;
use App\Tests\Integration\Infrastructure\AbstractUserRepository;

class UserDoctrineRepositoryTest extends AbstractUserRepository
{
    protected function createItemRepository(): UserRepositoryInterface
    {
        $repository = $this->getContainer()->get(UserDoctrineRepository::class);
        self::assertInstanceOf(UserRepositoryInterface::class, $repository);

        return $repository;
    }
}
