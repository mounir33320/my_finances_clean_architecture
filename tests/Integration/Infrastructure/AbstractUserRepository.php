<?php

namespace App\Tests\Integration\Infrastructure;

use App\Domain\User\Entity\User;
use App\Domain\User\Enum\RoleEnum;
use App\Domain\User\Repository\UserRepositoryInterface;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Zenstruck\Foundry\Test\Factories;
use Zenstruck\Foundry\Test\ResetDatabase;

abstract class AbstractUserRepository extends KernelTestCase
{
    use ResetDatabase;
    use Factories;

    abstract protected function createItemRepository(): UserRepositoryInterface;

    public function testCreateUser(): void
    {
        $repository = $this->createItemRepository();

        $repository->createUser($this->buildDomainUser());

        $this->assertSame(1, count($repository->getAll()));
    }

    public function testUserCreatedIsSame(): void
    {
        $repository = $this->createItemRepository();
        $domainUser = $this->buildDomainUser();
        $userCreated = $repository->createUser($domainUser);

        $this->assertSame($domainUser, $userCreated);
    }

    public function testGetUserByUuid(): void
    {
        $repository = $this->createItemRepository();

        $domainUser = $this->buildDomainUser();
        $repository->createUser($domainUser);

        $userFetched = $repository->getUserByUuid($domainUser->getUuid());
        $this->assertEquals($domainUser, $userFetched);
    }

    public function testGetUserByUuidReturnNullIfNotFound(): void
    {
        $repository = $this->createItemRepository();

        $repository->createUser($this->buildDomainUser());
        $userFetched = $repository->getUserByUuid('not-exists');
        $this->assertNull($userFetched);
    }

    public function testGetUserByUsername(): void
    {
        $repository = $this->createItemRepository();

        $domainUser = $this->buildDomainUser();
        $repository->createUser($domainUser);
        $userFetched = $repository->getUserByUsername($domainUser->getUsername());

        $this->assertEquals($domainUser, $userFetched);
    }

    public function testGetUserByUsernameReturnNullIfNotFound(): void
    {
        $repository = $this->createItemRepository();
        $domainUser = $this->buildDomainUser();
        $repository->createUser($domainUser);
        $userFetched = $repository->getUserByUsername('not-exists');
        $this->assertNull($userFetched);
    }

    public function testUpdateUser(): void
    {
        $repository = $this->createItemRepository();

        $domainUser = $this->buildDomainUser();
        $userCreated = $repository->createUser($domainUser);

        $userCreated->setUsername('username_updated');

        $userUpdated = $repository->updateUser($userCreated);
        $this->assertEquals($userCreated, $userUpdated);
    }

    public function testDeleteUser(): void
    {
        $repository = $this->createItemRepository();

        $domainUser = $this->buildDomainUser();

        $userCreated = $repository->createUser($domainUser);
        $repository->deleteUser($userCreated);

        $this->assertEquals(0, count($repository->getAll()));
    }

    private function buildDomainUser(): User
    {
        return new User('fake-uuid', 'user-test', 'fake-password', RoleEnum::RoleUser);
    }
}
