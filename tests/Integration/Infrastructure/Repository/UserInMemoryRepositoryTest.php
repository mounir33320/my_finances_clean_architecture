<?php

namespace App\Tests\Integration\Infrastructure\Repository;

use App\Domain\User\Repository\UserRepositoryInterface;
use App\Infrastructure\Repository\UserInMemoryRepository;
use App\Tests\Integration\Infrastructure\AbstractUserRepository;

class UserInMemoryRepositoryTest extends AbstractUserRepository
{
    protected function createItemRepository(): UserRepositoryInterface
    {
        return new UserInMemoryRepository();
    }
}
