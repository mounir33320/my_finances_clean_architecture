<?php

namespace App\Tests\Integration\Infrastructure\Repository;

use App\Domain\Debt\Repository\DebtRepositoryInterface;
use App\Infrastructure\Repository\DebtInMemoryRepository;
use App\Tests\Integration\Infrastructure\AbstractDebtRepository;

class DebtInMemoryRepositoryTest extends AbstractDebtRepository
{
    protected function createItemRepository(): DebtRepositoryInterface
    {
        return new DebtInMemoryRepository();
    }
}
