<?php

namespace App\Tests\Integration\Infrastructure;

use App\Domain\Debt\Entity\Debt;
use App\Domain\Debt\Repository\DebtRepositoryInterface;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Zenstruck\Foundry\Test\Factories;
use Zenstruck\Foundry\Test\ResetDatabase;

abstract class AbstractDebtRepository extends KernelTestCase
{
    use ResetDatabase;
    use Factories;

    private const DEBT_UUID = 'fake-uuid';
    private const USER_UUID = 'user-uuid';
    private const TOTAL_AMOUNT = 10000;
    private const CREDITOR_NAME = 'creditor-name';

    abstract protected function createItemRepository(): DebtRepositoryInterface;

    public function testCreateDebt(): void
    {
        $repository = $this->createItemRepository();

        $repository->createDebt($this->buildDebt());

        $this->assertSame(1, count($repository->getAllDebts(self::USER_UUID)));
    }

    public function testDebtCreatedIsSame(): void
    {
        $repository = $this->createItemRepository();
        $debt = $this->buildDebt();
        $debtCreated = $repository->createDebt($debt);

        $this->assertSame($debt, $debtCreated);
    }

    public function testGetDebtByUuid(): void
    {
        $repository = $this->createItemRepository();

        $debt = $this->buildDebt();
        $repository->createDebt($debt);

        $debtFetched = $repository->getDebtByUuidAndUserUuid($debt->getUuid(), $debt->getUserUuid());
        $this->assertEquals($debt, $debtFetched);
    }

    public function testGetDebtByUuidReturnNullIfNotFound(): void
    {
        $repository = $this->createItemRepository();

        $repository->createDebt($this->buildDebt());
        $debtFetched = $repository->getDebtByUuidAndUserUuid('not-exists', self::USER_UUID);
        $this->assertNull($debtFetched);
    }

    public function testUpdateDebt(): void
    {
        $repository = $this->createItemRepository();

        $debt = $this->buildDebt();
        $debtCreated = $repository->createDebt($debt);

        $updatedDebt = new Debt($debtCreated->getUuid(), $debtCreated->getUserUuid(), 5000, 'creditor-name-updated');

        $updatedDebt = $repository->updateDebt($updatedDebt);
        $this->assertEquals(
            new Debt($debtCreated->getUuid(), $debtCreated->getUserUuid(), 5000, 'creditor-name-updated'),
            $updatedDebt
        );
    }

    public function testDeleteUser(): void
    {
        $repository = $this->createItemRepository();

        $debt = $this->buildDebt();

        $debtCreated = $repository->createDebt($debt);
        $repository->deleteDebt($debtCreated);

        $this->assertCount(0, $repository->getAllDebts($debt->getUserUuid()));
    }

    private function buildDebt(): Debt
    {
        return new Debt(self::DEBT_UUID, self::USER_UUID, self::TOTAL_AMOUNT, self::CREDITOR_NAME);
    }
}
