<?php

namespace App\Tests\Unit\Application\User\CreateUser;

use App\Application\Shared\Error\Error;
use App\Application\Shared\Error\ErrorNotification;
use App\Application\User\CreateUser\CreateUser;
use App\Application\User\CreateUser\CreateUserInterface;
use App\Application\User\CreateUser\CreateUserPresenterInterface;
use App\Application\User\CreateUser\CreateUserRequest;
use App\Application\User\CreateUser\CreateUserResponse;
use App\Application\User\Shared\UserErrorMessageConstants;
use App\Domain\User\Entity\User;
use App\Domain\User\Enum\RoleEnum;
use App\Domain\User\Repository\UserRepositoryInterface;
use App\Infrastructure\Symfony\Service\UUIDGenerator;
use App\Shared\PasswordHasher;
use App\Infrastructure\Repository\UserInMemoryRepository;
use PHPUnit\Framework\TestCase;

class CreateUserTest extends TestCase implements CreateUserPresenterInterface
{
    private const VALID_PASSWORD = '123Tinaturner!';
    private const ROLE_USER = 'user';
    private CreateUserResponse $response;
    private UserRepositoryInterface $userRepository;
    private CreateUserInterface $createUser;

    protected function setUp(): void
    {
        $this->userRepository = new UserInMemoryRepository();
        $passwordHasher = new PasswordHasher();

        $this->createUser = new CreateUser(new ErrorNotification(), $this->userRepository, $passwordHasher, new UUIDGenerator());
    }

    public function testErrorIfCurrentUserIsNotAdmin(): void
    {
        $curentUser = $this->createCurrentUser(RoleEnum::RoleUser);
        $request = new CreateUserRequest(
            $curentUser->getUuid(),
            'john',
            self::VALID_PASSWORD,
            self::ROLE_USER
        );

        $this->createUser->execute($request, $this);

        $expectedNotif = (new ErrorNotification())->add(
            'role',
            UserErrorMessageConstants::CURRENT_USER_MUST_BE_ADMIN
        );

        $this->assertEquals($expectedNotif, $this->response->getErrorNotification());
    }

    public function testEmptyUsernameError(): void
    {
        $curentUser = $this->createCurrentUser(RoleEnum::RoleAdmin);
        $request = new CreateUserRequest(
            $curentUser->getUuid(),
            '',
            self::VALID_PASSWORD,
            self::ROLE_USER
        );

        $this->createUser->execute($request, $this);

        $expectedNotif = (new ErrorNotification())->add(
            'username',
            UserErrorMessageConstants::USERNAME_LENGTH_MUST_BE_GREATER_THAN_1
        );

        self::assertEquals($expectedNotif, $this->response->getErrorNotification());
    }

    public function testEmptyPasswordError(): void
    {
        $curentUser = $this->createCurrentUser(RoleEnum::RoleAdmin);
        $request = new CreateUserRequest($curentUser->getUuid(), 'john', '', self::ROLE_USER);
        $this->createUser->execute($request, $this);

        $expectedNotif = (new ErrorNotification())->add(
            'password',
            UserErrorMessageConstants::PASSWORD_CANNOT_BE_EMPTY
        );

        self::assertEquals($expectedNotif, $this->response->getErrorNotification());
    }

    public function testInvalidPasswordError(): void
    {
        $curentUser = $this->createCurrentUser(RoleEnum::RoleAdmin);
        $request = new CreateUserRequest(
            $curentUser->getUuid(),
            'john',
            'invalid-password',
            self::ROLE_USER
        );
        $this->createUser->execute($request, $this);

        $expectedNotif = (new ErrorNotification())->add(
            'password',
            UserErrorMessageConstants::PASSWORD_MUST_CONTAINS_LOWERCASE_LETTER_UPPERCASE_LETTER_NUMBER_SPECIAL_CHARACTER
        );

        self::assertEquals($expectedNotif, $this->response->getErrorNotification());
    }

    public function testInvalidPasswordWithUpperCaseLetterMissingError(): void
    {
        $curentUser = $this->createCurrentUser(RoleEnum::RoleAdmin);
        $request = new CreateUserRequest(
            $curentUser->getUuid(),
            'john',
            '123azerty!',
            self::ROLE_USER
        );
        $this->createUser->execute($request, $this);

        $expectedNotif = (new ErrorNotification())->add(
            'password',
            UserErrorMessageConstants::PASSWORD_MUST_CONTAINS_LOWERCASE_LETTER_UPPERCASE_LETTER_NUMBER_SPECIAL_CHARACTER
        );

        self::assertEquals($expectedNotif, $this->response->getErrorNotification());
    }

    public function testInvalidPasswordWithLowerCaseLetterMissingError(): void
    {
        $curentUser = $this->createCurrentUser(RoleEnum::RoleAdmin);
        $request = new CreateUserRequest(
            $curentUser->getUuid(),
            'john',
            '123AZERTY!',
            self::ROLE_USER
        );

        $this->createUser->execute($request, $this);

        $expectedNotif = (new ErrorNotification())->add(
            'password',
            UserErrorMessageConstants::PASSWORD_MUST_CONTAINS_LOWERCASE_LETTER_UPPERCASE_LETTER_NUMBER_SPECIAL_CHARACTER
        );

        self::assertEquals($expectedNotif, $this->response->getErrorNotification());
    }

    public function testInvalidPasswordWithSpecialCharacterMissingError(): void
    {
        $curentUser = $this->createCurrentUser(RoleEnum::RoleAdmin);
        $request = new CreateUserRequest(
            $curentUser->getUuid(),
            'john',
            '123AZERTYytreza',
            self::ROLE_USER
        );

        $this->createUser->execute($request, $this);

        $expectedNotif = (new ErrorNotification())->add(
            'password',
            UserErrorMessageConstants::PASSWORD_MUST_CONTAINS_LOWERCASE_LETTER_UPPERCASE_LETTER_NUMBER_SPECIAL_CHARACTER
        );

        self::assertEquals($expectedNotif, $this->response->getErrorNotification());
    }

    public function testInvalidPasswordWithLengthLessThan6CharactersError(): void
    {
        $curentUser = $this->createCurrentUser(RoleEnum::RoleAdmin);
        $request = new CreateUserRequest(
            $curentUser->getUuid(),
            'john',
            'Az6!',
            self::ROLE_USER
        );

        $this->createUser->execute($request, $this);

        $expectedNotif = (new ErrorNotification())->add(
            'password',
            UserErrorMessageConstants::PASSWORD_MUST_CONTAINS_LOWERCASE_LETTER_UPPERCASE_LETTER_NUMBER_SPECIAL_CHARACTER
        );

        self::assertEquals($expectedNotif, $this->response->getErrorNotification());
    }

    public function testValidPassword(): void
    {
        $curentUser = $this->createCurrentUser(RoleEnum::RoleAdmin);
        $request = new CreateUserRequest(
            $curentUser->getUuid(),
            'john',
            self::VALID_PASSWORD,
            self::ROLE_USER
        );

        $this->createUser->execute($request, $this);

        $errors = array_filter(
            $this->response->getErrorNotification()->getErrors(),
            fn (Error $error) => 'password' === $error->getFieldName()
        );

        self::assertEmpty($errors);
    }

    public function testNegativeIncomeError(): void
    {
        $curentUser = $this->createCurrentUser(RoleEnum::RoleAdmin);
        $request = new CreateUserRequest(
            $curentUser->getUuid(),
            'john',
            self::VALID_PASSWORD,
            self::ROLE_USER,
            -5
        );

        $this->createUser->execute($request, $this);

        $errors = array_filter(
            $this->response->getErrorNotification()->getErrors(),
            fn (Error $error) => 'income' === $error->getFieldName()
        );

        self::assertNotEmpty($errors);
        $error = array_values($errors)[0];

        self::assertSame(UserErrorMessageConstants::INCOME_CANNOT_BE_NEGATIVE, $error->getMessage());
    }

    public function testExistingUserError(): void
    {
        $currentUser = $this->createCurrentUser(RoleEnum::RoleAdmin);
        $existingUser = new User(
            $currentUser->getUuid(),
            'john',
            self::VALID_PASSWORD,
            RoleEnum::RoleUser
        );

        $this->userRepository->createUser($existingUser);

        $request = new CreateUserRequest(
            $currentUser->getUuid(),
            'john',
            self::VALID_PASSWORD,
            'user'
        );

        $this->createUser->execute($request, $this);

        $expectedNotif = (new ErrorNotification())->add('username', UserErrorMessageConstants::USER_ALREADY_EXISTS);

        $this->assertTrue($this->response->getErrorNotification()->hasError());
        $this->assertEquals($expectedNotif, $this->response->getErrorNotification());
    }

    public function testInvalidRole(): void
    {
        $currentUser = $this->createCurrentUser(RoleEnum::RoleAdmin);
        $request = new CreateUserRequest($currentUser->getUuid(), 'john', self::VALID_PASSWORD, 'invalid');
        $this->createUser->execute($request, $this);

        $expectedNotif = (new ErrorNotification())->add('role', UserErrorMessageConstants::INVALID_ROLE);

        $this->assertEquals($expectedNotif, $this->response->getErrorNotification());
    }

    public function testResponseReturnCreatedUser(): void
    {
        $currentUser = $this->createCurrentUser(RoleEnum::RoleAdmin);
        $request = new CreateUserRequest($currentUser->getUuid(), 'john', self::VALID_PASSWORD, 'user');
        $this->createUser->execute($request, $this);

        $createdUser = $this->response->getCreatedUser();
        $this->assertInstanceOf(User::class, $createdUser);
    }

    public function testCreatedUserUsernameAndRoleAreSame(): void
    {
        $currentUser = $this->createCurrentUser(RoleEnum::RoleAdmin);
        $request = new CreateUserRequest($currentUser->getUuid(), 'john', self::VALID_PASSWORD, 'user');
        $this->createUser->execute($request, $this);

        $createdUser = $this->response->getCreatedUser();

        $this->assertNotNull($createdUser);
        $this->assertSame('john', $createdUser->getUsername());
        $this->assertSame(RoleEnum::RoleUser, $createdUser->getRole());
    }

    public function present(CreateUserResponse $response): void
    {
        $this->response = $response;
    }

    private function createCurrentUser(RoleEnum $role): User
    {
        $user = new User('fake-uuid', 'blabla', 'password', $role);

        return $this->userRepository->createUser($user);
    }
}
