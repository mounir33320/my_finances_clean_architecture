<?php

namespace App\Tests\Unit\Application\User\Login;

use App\Application\Shared\Error\ErrorNotification;
use App\Application\User\Login\Login;
use App\Application\User\Login\LoginPresenterInterface;
use App\Application\User\Login\LoginRequest;
use App\Application\User\Login\LoginResponse;
use App\Application\User\Shared\UserErrorMessageConstants;
use App\Domain\User\Entity\User;
use App\Domain\User\Enum\RoleEnum;
use App\Domain\User\Repository\UserRepositoryInterface;
use App\Shared\PasswordHasher;
use App\Infrastructure\Repository\UserInMemoryRepository;
use PHPUnit\Framework\TestCase;

class LoginTest extends TestCase implements LoginPresenterInterface
{
    private const PASSWORD = 'password';

    private Login $login;
    private LoginResponse $response;
    private UserRepositoryInterface $userRepository;

    private User $registeredUser;

    protected function setUp(): void
    {
        $this->userRepository = new UserInMemoryRepository();
        $passwordHasher = new PasswordHasher();

        $this->login = new Login(new ErrorNotification(), $this->userRepository, $passwordHasher);

        $this->registeredUser = new User('123456', 'mounir', $passwordHasher->hash(self::PASSWORD), RoleEnum::RoleUser);
    }

    public function present(LoginResponse $response): void
    {
        $this->response = $response;
    }

    public function testErrorUsernameNotExists(): void
    {
        $request = new LoginRequest('unknown', 'password');
        $this->login->execute($request, $this);

        $expectedNotif = (new ErrorNotification())->add('username', UserErrorMessageConstants::UNKNOWN_USERNAME);

        $this->assertEquals($expectedNotif, $this->response->getNotification());
    }

    public function testWrongPassword(): void
    {
        $request = new LoginRequest('mounir', 'wrong_password');
        $this->userRepository->createUser($this->registeredUser);
        $this->login->execute($request, $this);

        $expectedNotif = (new ErrorNotification())->add('password', UserErrorMessageConstants::INVALID_CREDENTIALS);

        $this->assertEquals($expectedNotif, $this->response->getNotification());
    }

    public function testUserIsSetToResponse(): void
    {
        $request = new LoginRequest('mounir', self::PASSWORD);
        $this->userRepository->createUser($this->registeredUser);
        $this->login->execute($request, $this);

        $this->assertSame($this->registeredUser, $this->response->getUser());
    }
}
