<?php

namespace App\Tests\Unit\Application\User\DeleteUser;

use App\Application\Shared\Error\ErrorNotification;
use App\Application\Shared\Error\ErrorNotificationInterface;
use App\Application\User\DeleteUser\DeleteUser;
use App\Application\User\DeleteUser\DeleteUserInterface;
use App\Application\User\DeleteUser\DeleteUserPresenterInterface;
use App\Application\User\DeleteUser\DeleteUserRequest;
use App\Application\User\DeleteUser\DeleteUserResponse;
use App\Application\User\Shared\UserErrorMessageConstants;
use App\Domain\User\Entity\User;
use App\Domain\User\Enum\RoleEnum;
use App\Domain\User\Repository\UserRepositoryInterface;
use App\Infrastructure\Repository\UserInMemoryRepository;
use PHPUnit\Framework\TestCase;

class DeleteUserTest extends TestCase implements DeleteUserPresenterInterface
{
    private DeleteUserResponse $response;
    private UserRepositoryInterface $userRepository;
    private DeleteUserInterface $deleteUser;
    private ErrorNotificationInterface $errorNotification;

    public function present(DeleteUserResponse $response): void
    {
        $this->response = $response;
    }

    public function testUserWithUserRoleCannotDeleteAnotherUser(): void
    {
        $user = $this->createUser('uuid1', 'user', 'password', RoleEnum::RoleUser);
        $userToDelete = $this->createUser('uuid2', 'user', 'password', RoleEnum::RoleUser);
        $request = new DeleteUserRequest($user->getUuid(), $userToDelete->getUuid());
        $this->deleteUser->execute($request, $this);

        $expectedErrorNotif = (new ErrorNotification())->add('role', UserErrorMessageConstants::CURRENT_USER_MUST_BE_ADMIN);

        $this->assertEquals($expectedErrorNotif, $this->response->getErrorNotification());
    }

    public function testUserAdminCannotDeleteAnotherUserAdmin(): void
    {
        $adminUser = $this->createUser('uuid1', 'admin', 'password', RoleEnum::RoleAdmin);
        $userAdminToDelete = $this->createUser('uuid2', 'admin', 'password', RoleEnum::RoleAdmin);
        $request = new DeleteUserRequest($adminUser->getUuid(), $userAdminToDelete->getUuid());
        $this->deleteUser->execute($request, $this);

        $expectedErrorNotif = (new ErrorNotification())->add('role', UserErrorMessageConstants::ADMIN_USER_CANNOT_BE_DELETED_BY_AN_ANOTHER_USER);

        $this->assertEquals($expectedErrorNotif, $this->response->getErrorNotification());
    }

    public function testUserAdminCanDeleteAnotherUser(): void
    {
        $adminUser = $this->createUser('uuid1', 'admin', 'password', RoleEnum::RoleAdmin);
        $userToDelete = $this->createUser('uuid2', 'user', 'password', RoleEnum::RoleUser);
        $request = new DeleteUserRequest($adminUser->getUuid(), $userToDelete->getUuid());
        $this->deleteUser->execute($request, $this);

        $this->assertFalse($this->response->getErrorNotification()->hasError());
    }

    public function testUserCanDeleteHimSelf(): void
    {
        $user = $this->createUser('uuid1', 'user', 'password', RoleEnum::RoleAdmin);
        $userToDelete = $this->createUser('uuid2', 'user', 'password', RoleEnum::RoleUser);
        $request = new DeleteUserRequest($user->getUuid(), $userToDelete->getUuid());
        $this->deleteUser->execute($request, $this);

        $this->assertFalse($this->response->getErrorNotification()->hasError());
    }

    protected function setUp(): void
    {
        $this->userRepository = new UserInMemoryRepository();
        $this->errorNotification = new ErrorNotification();
        $this->deleteUser = new DeleteUser($this->userRepository, $this->errorNotification);
    }

    private function createUser(string $uuid, string $username, string $password, RoleEnum $role, ?int $income = null): User
    {
        $user = new User($uuid, $username, $password, $role, $income);

        return $this->userRepository->createUser($user);
    }
}
