<?php

namespace App\Tests\Unit\Application\User\UpdateUser;

use App\Application\Shared\Error\ErrorNotification;
use App\Application\User\Shared\UserErrorMessageConstants;
use App\Application\User\UpdateUser\UpdateUser;
use App\Application\User\UpdateUser\UpdateUserPresenterInterface;
use App\Application\User\UpdateUser\UpdateUserRequest;
use App\Application\User\UpdateUser\UpdateUserResponse;
use App\Domain\User\Entity\User;
use App\Domain\User\Enum\RoleEnum;
use App\Domain\User\Repository\UserRepositoryInterface;
use App\Infrastructure\Repository\UserInMemoryRepository;
use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\TestCase;

class UpdateUserTest extends TestCase implements UpdateUserPresenterInterface
{
    private UpdateUserResponse $response;
    private UpdateUser $updateUser;
    private UserRepositoryInterface $userRepository;

    public function present(UpdateUserResponse $response): void
    {
        $this->response = $response;
    }

    protected function setUp(): void
    {
        $this->userRepository = new UserInMemoryRepository();
        $this->updateUser = new UpdateUser($this->userRepository, new ErrorNotification());
    }

    #[DataProvider('usernameProvider')]
    public function testUsernameLengthErrors(string $username, bool $expectedError): void
    {
        $currentUser = new User('currentUserAdminId', 'john', '12345', RoleEnum::RoleAdmin);
        $currentUser = $this->userRepository->createUser($currentUser);

        $userToUpdate = new User('userToUpdate', 'john', '12345', RoleEnum::RoleUser);
        $this->userRepository->createUser($userToUpdate);

        $request = new UpdateUserRequest($currentUser->getUuid(), $userToUpdate->getUuid(), $username, 'user');
        $this->updateUser->execute($request, $this);

        if ($expectedError) {
            $this->assertTrue($this->response->getErrorNotification()->hasError());
        } else {
            $this->assertFalse($this->response->getErrorNotification()->hasError());
        }
    }

    public function testCurrentUserIsNotAdminErrorToUpdateAnAnotherUser(): void
    {
        $currentUser = new User('currentUserNotAdminId', 'john', '12345', RoleEnum::RoleUser);
        $currentUser = $this->userRepository->createUser($currentUser);

        $request = new UpdateUserRequest($currentUser->getUuid(), 'uuid', 'username', 'user');

        $expectedNotif = (new ErrorNotification())->add('role', UserErrorMessageConstants::CURRENT_USER_MUST_BE_ADMIN);
        $this->updateUser->execute($request, $this);

        $this->assertEquals($expectedNotif, $this->response->getErrorNotification());
    }

    public function testCurrentUserIsAdminToUpdateAnAnotherUser(): void
    {
        $currentUser = new User('currentUserNotAdminId', 'john', '12345', RoleEnum::RoleAdmin);
        $currentUser = $this->userRepository->createUser($currentUser);

        $userToUpdate = new User('userToUpdate', 'john', '12345', RoleEnum::RoleUser);
        $this->userRepository->createUser($userToUpdate);

        $request = new UpdateUserRequest($currentUser->getUuid(), $userToUpdate->getUuid(), 'username', 'user');
        $this->updateUser->execute($request, $this);

        $this->assertFalse($this->response->getErrorNotification()->hasError());
    }

    public function testUserToUpdateNotFoundError(): void
    {
        $currentUser = new User('currentUserNotAdminId', 'john', '12345', RoleEnum::RoleAdmin);
        $currentUser = $this->userRepository->createUser($currentUser);

        $request = new UpdateUserRequest($currentUser->getUuid(), 'uuid', 'username', 'user');
        $this->updateUser->execute($request, $this);

        $expectedNotif = (new ErrorNotification())->add('user', UserErrorMessageConstants::USER_NOT_FOUND);

        $this->assertEquals($expectedNotif, $this->response->getErrorNotification());
    }

    public function testUserToUpdateIsNotAnAdminUser(): void
    {
        $currentUser = new User('currentUserAdminId', 'john', '12345', RoleEnum::RoleAdmin);
        $userToUpdate = new User('userToUpdateId', 'jane', '12345', RoleEnum::RoleAdmin);

        $currentUser = $this->userRepository->createUser($currentUser);
        $userToUpdate = $this->userRepository->createUser($userToUpdate);

        $request = new UpdateUserRequest($currentUser->getUuid(), $userToUpdate->getUuid(), $userToUpdate->getUsername(), 'admin');
        $this->updateUser->execute($request, $this);

        $expectedNotif = (new ErrorNotification())->add('role', UserErrorMessageConstants::ADMIN_USER_CANNOT_BE_UPDATED_BY_AN_ANOTHER_USER);

        $this->assertEquals($expectedNotif, $this->response->getErrorNotification());
    }

    public function testInvalidRole(): void
    {
        $currentUser = new User('currentUserAdminId', 'john', '12345', RoleEnum::RoleAdmin);

        $request = new UpdateUserRequest($currentUser->getUuid(), 'john', 'username', 'invalid');
        $this->updateUser->execute($request, $this);

        $expectedNotif = (new ErrorNotification())->add('role', UserErrorMessageConstants::INVALID_ROLE);

        $this->assertEquals($expectedNotif, $this->response->getErrorNotification());
    }

    public function testCurrentUserIsSameWithUserToUpdate(): void
    {
        $userToUpdate = new User('userToUpdateId', 'jane', '12345', RoleEnum::RoleUser);
        $userToUpdate = $this->userRepository->createUser($userToUpdate);

        $request = new UpdateUserRequest($userToUpdate->getUuid(), $userToUpdate->getUuid(), 'updated-user', 'user');
        $this->updateUser->execute($request, $this);

        $expectedUpdatedUser = $userToUpdate->setUsername($request->username);

        $this->assertEquals($expectedUpdatedUser, $this->response->getUpdatedUser());
    }

    public function testNegativeIncomeError(): void
    {
        $userToUpdate = new User('userToUpdateId', 'jane', '12345', RoleEnum::RoleUser);
        $userToUpdate = $this->userRepository->createUser($userToUpdate);

        $request = new UpdateUserRequest($userToUpdate->getUuid(), $userToUpdate->getUuid(), 'updated-user', 'user', -500);
        $this->updateUser->execute($request, $this);

        $expectedNotif = (new ErrorNotification())->add('income', UserErrorMessageConstants::INCOME_CANNOT_BE_NEGATIVE);
        $this->assertEquals($expectedNotif, $this->response->getErrorNotification());
    }

    public function testUserUpdate(): void
    {
        $currentUser = new User('currentUserAdminId', 'john', '12345', RoleEnum::RoleAdmin);
        $userToUpdate = new User('userToUpdateId', 'jane', '12345', RoleEnum::RoleUser);

        $currentUser = $this->userRepository->createUser($currentUser);
        $userToUpdate = $this->userRepository->createUser($userToUpdate);

        $request = new UpdateUserRequest($currentUser->getUuid(), $userToUpdate->getUuid(), 'updated', 'user', 2500);
        $this->updateUser->execute($request, $this);

        $expectedUser = new User($request->uuid, $request->username, $userToUpdate->getPassword(), RoleEnum::RoleUser, $request->income);

        $this->assertEquals($expectedUser, $this->response->getUpdatedUser());
    }

    /**
     * @return array<string, array{0:string, 1:bool}>
     */
    public static function usernameProvider(): array
    {
        return [
            'Empty username' => ['', true],
            'One character username' => ['a', true],
            'Valid username' => ['Jane', false],
        ];
    }
}
