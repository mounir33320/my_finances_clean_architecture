<?php

namespace App\Tests\Unit\Application\Debt\UpdateDebt;

use App\Application\Debt\Shared\DebtErrorMessageConstants;
use App\Application\Debt\UpdateDebt\UpdateDebt;
use App\Application\Debt\UpdateDebt\UpdateDebtInterface;
use App\Application\Debt\UpdateDebt\UpdateDebtPresenterInterface;
use App\Application\Debt\UpdateDebt\UpdateDebtRequest;
use App\Application\Debt\UpdateDebt\UpdateDebtResponse;
use App\Application\Shared\Error\ErrorNotification;
use App\Application\User\Shared\UserErrorMessageConstants;
use App\Domain\Debt\Entity\Debt;
use App\Domain\Debt\Repository\DebtRepositoryInterface;
use App\Domain\User\Entity\User;
use App\Domain\User\Enum\RoleEnum;
use App\Domain\User\Repository\UserRepositoryInterface;
use App\Infrastructure\Repository\DebtInMemoryRepository;
use App\Infrastructure\Repository\UserInMemoryRepository;
use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\TestCase;

class UpdateDebtTest extends TestCase implements UpdateDebtPresenterInterface
{
    private const CURRENT_USER_UUID = 'current-user-uuid';
    private const DEBT_UUID = 'debt-uuid';
    private const TOTAL_AMOUNT = 10000;
    private const CREDITOR_NAME = 'creditor-name';
    private const NEGATIVE_TOTAL_AMOUNT = -100;
    private const TOTAL_AMOUNT_GREATER_THAN_1_MILLION = 100000000;

    private UpdateDebtResponse $response;

    private UpdateDebtInterface $updateDebt;

    private DebtRepositoryInterface $debtRepository;
    private UserRepositoryInterface $userRepository;

    public function present(UpdateDebtResponse $response): void
    {
        $this->response = $response;
    }

    public function testDebtNotFound(): void
    {
        $request = new UpdateDebtRequest(self::CURRENT_USER_UUID, self::DEBT_UUID, self::TOTAL_AMOUNT, self::CREDITOR_NAME);

        $this->updateDebt->execute($request, $this);

        $expectedNotif = (new ErrorNotification())->add('uuid', DebtErrorMessageConstants::DEBT_NOT_FOUND);

        $this->assertEquals($expectedNotif, $this->response->getErrorNotification());
    }

    public function testUserNotFound(): void
    {
        $userUuid = 'not-existing-user';
        $debt = new Debt(self::DEBT_UUID, $userUuid, self::TOTAL_AMOUNT, self::CREDITOR_NAME);
        $this->debtRepository->createDebt($debt);

        $request = new UpdateDebtRequest($userUuid, self::DEBT_UUID, self::TOTAL_AMOUNT, self::CREDITOR_NAME);

        $this->updateDebt->execute($request, $this);

        $expectedNotif = (new ErrorNotification())->add('userUuid', UserErrorMessageConstants::USER_NOT_FOUND);

        $this->assertEquals($expectedNotif, $this->response->getErrorNotification());
    }

    public function testErrorWhenUpdateDebtBelongingAnotherUser(): void
    {
        $currentUser = new User(self::CURRENT_USER_UUID, 'john', 'password', RoleEnum::RoleUser);
        $this->userRepository->createUser($currentUser);
        $currentUserDebt = new Debt(self::DEBT_UUID, $currentUser->getUuid(), self::TOTAL_AMOUNT, self::CREDITOR_NAME);
        $this->debtRepository->createDebt($currentUserDebt);

        $anotherUser = new User('another-uuid-user', 'bob', 'password', RoleEnum::RoleUser);
        $this->userRepository->createUser($anotherUser);

        $request = new UpdateDebtRequest($anotherUser->getUuid(), $currentUserDebt->getUuid(), self::TOTAL_AMOUNT, self::CREDITOR_NAME);
        $this->updateDebt->execute($request, $this);

        $expectedNotif = (new ErrorNotification())->add('uuid', DebtErrorMessageConstants::DEBT_NOT_FOUND);

        $this->assertEquals($expectedNotif, $this->response->getErrorNotification());
    }

    #[DataProvider('emptyValuesProvider')]
    public function testEmptyValues(
        string $userUuid,
        string $debtUuid,
        string $creditorName,
        string $field,
        string $message
    ): void {
        $currentUser = new User(self::CURRENT_USER_UUID, 'john', 'password', RoleEnum::RoleUser);
        $this->userRepository->createUser($currentUser);

        $debt = new Debt(self::DEBT_UUID, self::CURRENT_USER_UUID, self::TOTAL_AMOUNT, self::CREDITOR_NAME);
        $this->debtRepository->createDebt($debt);

        $request = new UpdateDebtRequest($userUuid, $debtUuid, self::TOTAL_AMOUNT, $creditorName);

        $this->updateDebt->execute($request, $this);

        $expectedNotif = (new ErrorNotification())->add($field, $message);

        $this->assertEquals($expectedNotif, $this->response->getErrorNotification());
    }

    public function testNegativeTotalAmount(): void
    {
        $request = new UpdateDebtRequest(self::CURRENT_USER_UUID, self::DEBT_UUID, self::NEGATIVE_TOTAL_AMOUNT, self::CREDITOR_NAME);
        $this->updateDebt->execute($request, $this);

        $expectedErrorNotification = (new ErrorNotification())->add('totalAmount', DebtErrorMessageConstants::TOTAL_AMOUNT_CANNOT_BE_NEGATIVE);
        $this->assertEquals($expectedErrorNotification, $this->response->getErrorNotification());
    }

    public function testTotalAmountGreaterThan1Million(): void
    {
        $request = new UpdateDebtRequest(self::CURRENT_USER_UUID, self::DEBT_UUID, self::TOTAL_AMOUNT_GREATER_THAN_1_MILLION, self::CREDITOR_NAME);
        $this->updateDebt->execute($request, $this);

        $expectedErrorNotification = (new ErrorNotification())->add('totalAmount', DebtErrorMessageConstants::TOTAL_AMOUNT_CANNOT_BE_GREATER_THAN_1_MILLION);
        $this->assertEquals($expectedErrorNotification, $this->response->getErrorNotification());
    }

    public function testUpdateDebt(): void
    {
        $currentUser = new User(self::CURRENT_USER_UUID, 'john', 'password', RoleEnum::RoleUser);
        $this->userRepository->createUser($currentUser);

        $debt = new Debt(self::DEBT_UUID, self::CURRENT_USER_UUID, self::TOTAL_AMOUNT, self::CREDITOR_NAME);
        $this->debtRepository->createDebt($debt);

        $request = new UpdateDebtRequest(self::CURRENT_USER_UUID, self::DEBT_UUID, 5000, 'creditor-name-updated');

        $this->updateDebt->execute($request, $this);

        $this->assertFalse($this->response->getErrorNotification()->hasError());

        $expectedResponse = new UpdateDebtResponse(new ErrorNotification());
        $expectedResponse->setUpdatedDebt(new Debt($request->debtUuid, $request->currentUserUuid, $request->totalAmount, $request->creditorName));

        $this->assertEquals($expectedResponse, $this->response);
    }

    protected function setUp(): void
    {
        $this->debtRepository = new DebtInMemoryRepository();
        $this->userRepository = new UserInMemoryRepository();

        $this->updateDebt = new UpdateDebt($this->debtRepository, $this->userRepository);
    }

    /**
     * @return array<string, array{0:string, 1:string, 2:string, 3:string, 4:string}>
     */
    public static function emptyValuesProvider(): array
    {
        return [
            'Empty debt uuid' => [self::CURRENT_USER_UUID, '', self::CREDITOR_NAME, 'uuid', DebtErrorMessageConstants::DEBT_UUID_CANNOT_BE_EMPTY],
            'Empty user uuid' => ['', self::DEBT_UUID, self::CREDITOR_NAME, 'userUuid', DebtErrorMessageConstants::USER_UUID_CANNOT_BE_EMPTY],
            'Empty creditor name' => [self::CURRENT_USER_UUID, self::DEBT_UUID, '', 'creditorName', DebtErrorMessageConstants::CREDITOR_NAME_CANNOT_BE_EMPTY],
        ];
    }
}
