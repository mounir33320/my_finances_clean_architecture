<?php

namespace App\Tests\Unit\Application\Debt\GetListOfDebts;

use App\Application\Debt\GetListOfDebts\GetListOfDebts;
use App\Application\Debt\GetListOfDebts\GetListOfDebtsInterface;
use App\Application\Debt\GetListOfDebts\GetListOfDebtsPresenterInterface;
use App\Application\Debt\GetListOfDebts\GetListOfDebtsRequest;
use App\Application\Debt\GetListOfDebts\GetListOfDebtsResponse;
use App\Application\Debt\Shared\DebtErrorMessageConstants;
use App\Application\Shared\Error\ErrorNotification;
use App\Domain\Debt\Repository\DebtRepositoryInterface;
use App\Infrastructure\Repository\DebtInMemoryRepository;
use PHPUnit\Framework\TestCase;

class GetListOfDebtsTest extends TestCase implements GetListOfDebtsPresenterInterface
{
    private DebtRepositoryInterface $debtRepository;
    private GetListOfDebtsInterface $getListOfDebts;
    private GetListOfDebtsResponse $response;

    public function present(GetListOfDebtsResponse $response): void
    {
        $this->response = $response;
    }

    public function testErrorWhenCurrentUserUuidIsEmpty(): void
    {
        $request = new GetListOfDebtsRequest('');
        $this->getListOfDebts->execute($request, $this);

        $expectedErrorNotif = (new ErrorNotification())->add('user', DebtErrorMessageConstants::USER_UUID_CANNOT_BE_EMPTY);

        $this->assertEquals($expectedErrorNotif, $this->response->getErrorNotification());
    }

    protected function setUp(): void
    {
        $this->debtRepository = new DebtInMemoryRepository();
        $this->getListOfDebts = new GetListOfDebts($this->debtRepository);
    }
}
