<?php

namespace App\Tests\Unit\Application\Debt\RemoveDebt;

use App\Application\Debt\RemoveDebt\RemoveDebt;
use App\Application\Debt\RemoveDebt\RemoveDebtInterface;
use App\Application\Debt\RemoveDebt\RemoveDebtPresenterInterface;
use App\Application\Debt\RemoveDebt\RemoveDebtRequest;
use App\Application\Debt\RemoveDebt\RemoveDebtResponse;
use App\Application\Debt\Shared\DebtErrorMessageConstants;
use App\Application\Shared\Error\ErrorNotification;
use App\Domain\Debt\Repository\DebtRepositoryInterface;
use App\Infrastructure\Repository\DebtInMemoryRepository;
use PHPUnit\Framework\TestCase;

class RemoveDebtTest extends TestCase implements RemoveDebtPresenterInterface
{
    private RemoveDebtInterface $removeDebt;
    private RemoveDebtResponse $response;
    private DebtRepositoryInterface $debtRepository;

    public function present(RemoveDebtResponse $response): void
    {
        $this->response = $response;
    }

    public function testNotFoundDebtError(): void
    {
        $userUuid = 'userUuid';
        $debtUuid = 'not-found-uuid';

        $request = new RemoveDebtRequest($debtUuid, $userUuid);
        $this->removeDebt->execute($request, $this);

        $expectedErrorNotif = (new ErrorNotification())->add('uuid', DebtErrorMessageConstants::DEBT_NOT_FOUND);

        $this->assertEquals($expectedErrorNotif, $this->response->getErrorNotification());
    }

    protected function setUp(): void
    {
        $this->debtRepository = new DebtInMemoryRepository();
        $this->removeDebt = new RemoveDebt($this->debtRepository);
    }
}
