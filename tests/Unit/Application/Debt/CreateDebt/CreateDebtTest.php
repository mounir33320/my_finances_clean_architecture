<?php

namespace App\Tests\Unit\Application\Debt\CreateDebt;

use App\Application\Debt\CreateDebt\CreateDebt;
use App\Application\Debt\CreateDebt\CreateDebtInterface;
use App\Application\Debt\CreateDebt\CreateDebtPresenterInterface;
use App\Application\Debt\CreateDebt\CreateDebtRequest;
use App\Application\Debt\CreateDebt\CreateDebtResponse;
use App\Application\Service\UUIDGeneratorInterface;
use App\Application\Shared\Error\ErrorNotification;
use App\Domain\Debt\Repository\DebtRepositoryInterface;
use App\Domain\User\Entity\User;
use App\Domain\User\Enum\RoleEnum;
use App\Domain\User\Repository\UserRepositoryInterface;
use App\Infrastructure\Repository\DebtInMemoryRepository;
use App\Infrastructure\Repository\UserInMemoryRepository;
use PHPUnit\Framework\TestCase;
use PHPUnit\Framework\Attributes\DataProvider;

class CreateDebtTest extends TestCase implements CreateDebtPresenterInterface
{
    private const USER_FAKE_UUID = 'user_fake_uuid';
    private const CREDITOR_NAME = 'John Doe';
    private const NEGATIVE_TOTAL_AMOUNT = -100;
    private const TOTAL_AMOUNT_GREATER_THAN_1_MILLION = 100000000;
    private const CORRECT_TOTAL_AMOUNT = 1000000;

    private CreateDebtResponse $response;
    private DebtRepositoryInterface $repository;
    private CreateDebtInterface $createDebt;

    private UserRepositoryInterface $userRepository;

    public function present(CreateDebtResponse $response): void
    {
        $this->response = $response;
    }

    public function testNegativeTotalAmount(): void
    {
        $request = new CreateDebtRequest(self::USER_FAKE_UUID, self::NEGATIVE_TOTAL_AMOUNT, self::CREDITOR_NAME);
        $this->createDebt->execute($request, $this);

        $expectedErrorNotification = (new ErrorNotification())->add('totalAmount', 'total_amount_cannot_be_negative');
        $this->assertEquals($expectedErrorNotification, $this->response->getErrorNotification());
    }

    public function testTotalAmountGreaterThan1Million(): void
    {
        $request = new CreateDebtRequest(self::USER_FAKE_UUID, self::TOTAL_AMOUNT_GREATER_THAN_1_MILLION, self::CREDITOR_NAME);
        $this->createDebt->execute($request, $this);

        $expectedErrorNotification = (new ErrorNotification())->add('totalAmount', 'total_amount_cannot_be_greater_than_1_million');
        $this->assertEquals($expectedErrorNotification, $this->response->getErrorNotification());
    }

    #[DataProvider('emptyStringValuesProvider')]
    public function testEmptyValues(
        string $userUuid,
        int $totalAmount,
        string $creditorName,
        string $field,
        string $expectedErrorString
    ): void {
        $request = new CreateDebtRequest($userUuid, $totalAmount, $creditorName);
        $this->createDebt->execute($request, $this);

        $expectedErrorNotification = (new ErrorNotification())->add($field, $expectedErrorString);
        $this->assertEquals($expectedErrorNotification, $this->response->getErrorNotification());
    }

    public function testUserNotFound(): void
    {
        $request = new CreateDebtRequest('not-existing', self::CORRECT_TOTAL_AMOUNT, self::CREDITOR_NAME);
        $this->createDebt->execute($request, $this);

        $expectedErrorNotification = (new ErrorNotification())->add('userUuid', 'user_not_found');
        $this->assertEquals($expectedErrorNotification, $this->response->getErrorNotification());
    }

    public function testCreateDebt(): void
    {
        $user = new User(self::USER_FAKE_UUID, 'john', 'password', RoleEnum::RoleUser);
        $this->userRepository->createUser($user);
        $request = new CreateDebtRequest(self::USER_FAKE_UUID, self::CORRECT_TOTAL_AMOUNT, self::CREDITOR_NAME);
        $this->createDebt->execute($request, $this);

        $this->assertFalse($this->response->getErrorNotification()->hasError());
    }

    protected function setUp(): void
    {
        $this->repository = new DebtInMemoryRepository();
        $this->userRepository = new UserInMemoryRepository();
        $uuidGenerator = $this->createMock(UUIDGeneratorInterface::class);
        $this->createDebt = new CreateDebt($uuidGenerator, $this->repository, $this->userRepository);
    }

    /**
     * @return array<string, array{0:string, 1:int, 2:string, 3:string, 4:string}>
     */
    public static function emptyStringValuesProvider(): array
    {
        $emptyUserUuidError = 'user_uuid_cannot_be_empty';
        $emptyCreditorNameError = 'creditor_name_cannot_be_empty';

        return [
            'Empty user UUID' => ['', self::CORRECT_TOTAL_AMOUNT, self::CREDITOR_NAME, 'userUuid', $emptyUserUuidError],
            'Empty creditor name' => [self::USER_FAKE_UUID, self::CORRECT_TOTAL_AMOUNT, '', 'creditorName', $emptyCreditorNameError],
        ];
    }
}
