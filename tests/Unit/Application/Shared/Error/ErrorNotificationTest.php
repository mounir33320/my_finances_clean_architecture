<?php

namespace App\Tests\Unit\Application\Shared\Error;

use App\Application\Shared\Error\Error;
use App\Application\Shared\Error\ErrorNotification;
use App\Application\Shared\Error\ErrorNotificationInterface;
use PHPUnit\Framework\TestCase;

class ErrorNotificationTest extends TestCase
{
    private ErrorNotificationInterface $errorNotification;

    protected function setUp(): void
    {
        $this->errorNotification = new ErrorNotification();
    }

    public function testAddError(): void
    {
        $field = 'test-field';
        $message = 'test-message';

        $expectedError = new Error($field, $message);

        $this->errorNotification->add($field, $message);

        self::assertEquals($expectedError, $this->errorNotification->getErrors()[0]);
        self::assertTrue($this->errorNotification->hasError());
    }
}
