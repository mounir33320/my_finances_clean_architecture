<?php

namespace App\Tests\Unit\Domain\Debt\Entity;

use App\Domain\Debt\Entity\Debt;
use App\Domain\Debt\Entity\Payment;
use App\Domain\Debt\Exception\PaymentNotFoundException;
use PHPUnit\Framework\TestCase;

class DebtTest extends TestCase
{
    public function testAddPayment(): void
    {
        $debt = new Debt('uuid', 'userUuid', 100, 'John Doe');

        $paymentUuid = 'payment-uuid';
        $paymentDate = new \DateTimeImmutable();
        $debt->addPayment($paymentUuid, 50, $paymentDate);

        $this->assertEquals(new Payment($paymentUuid, $debt->getUuid(), 50, $paymentDate), $debt->getPayments()[$paymentUuid]);
    }

    public function testUpdatePayment(): void
    {
        $debt = new Debt('uuid', 'userUuid', 100, 'John Doe');
        $paymentUuid = 'payment-uuid';
        $paymentDate = new \DateTimeImmutable();
        $debt->addPayment($paymentUuid, 50, $paymentDate);

        $debt->updatePayment($paymentUuid, 60, $paymentDate);

        $expectedPayment = new Payment($paymentUuid, $debt->getUuid(), 60, $paymentDate);

        $this->assertEquals($expectedPayment, $debt->getPayments()[$paymentUuid]);
    }

    public function testUpdateNonExistentPayment(): void
    {
        $debt = new Debt('uuid', 'userUuid', 100, 'John Doe');
        $paymentUuid = 'payment-uuid';

        $this->expectException(PaymentNotFoundException::class);
        $debt->updatePayment($paymentUuid, 60);
    }

    public function testRemovePayment(): void
    {
        $debt = new Debt('uuid', 'userUuid', 100, 'John Doe');
        $paymentUuid = 'payment-uuid';
        $paymentDate = new \DateTimeImmutable();
        $debt->addPayment($paymentUuid, 50, $paymentDate);

        $debt->removePayment($paymentUuid);

        $this->assertArrayNotHasKey($paymentUuid, $debt->getPayments());
    }

    public function testRemoveNonExistentPayment(): void
    {
        $debt = new Debt('uuid', 'userUuid', 100, 'John Doe');
        $paymentUuid = 'payment-uuid';

        $this->expectException(PaymentNotFoundException::class);
        $debt->removePayment($paymentUuid);
    }

    public function testGetRemainingAmount(): void
    {
        $debt = new Debt('uuid', 'userUuid', 10000, 'John Doe');
        $debt->addPayment('uuid1', 1000);
        $debt->addPayment('uuid2', 2000);
        $debt->addPayment('uuid3', 3000);

        $expectedAmount = 10000 - 1000 - 2000 - 3000;
        $this->assertSame($expectedAmount, $debt->getRemainingAmount());
    }

    public function testGetRemainingAmountPercentage(): void
    {
        $debt = new Debt('uuid', 'userUuid', 10000, 'John Doe');
        $debt->addPayment('uuid1', 1000);
        $debt->addPayment('uuid2', 2000);
        $debt->addPayment('uuid3', 3000);

        $remainingAmount = 10000 - 1000 - 2000 - 3000;

        $expectedPercentage = (int) round($remainingAmount / 10000 * 100);
        $this->assertSame($expectedPercentage, $debt->getRemainingAmountPercentage());
    }

    public function testGetPaidAmount(): void
    {
        $debt = new Debt('uuid', 'userUuid', 10000, 'John Doe');
        $debt->addPayment('uuid1', 1000);
        $debt->addPayment('uuid2', 2000);
        $debt->addPayment('uuid3', 3000);

        $expectedAmount = 1000 + 2000 + 3000;
        $this->assertSame($expectedAmount, $debt->getPaidAmount());
    }

    public function testGetPaidAmountPercentage(): void
    {
        $debt = new Debt('uuid', 'userUuid', 10000, 'John Doe');
        $debt->addPayment('uuid1', 1000);
        $debt->addPayment('uuid2', 2000);
        $debt->addPayment('uuid3', 3000);

        $paidAmount = 1000 + 2000 + 3000;
        $expectedPercentage = (int) round($paidAmount / 10000 * 100);

        $this->assertSame($expectedPercentage, $debt->getPaidAmountPercentage());
    }
}
