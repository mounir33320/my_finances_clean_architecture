<?php

namespace App\Shared;

use App\Application\User\Service\PasswordHasherInterface;

class PasswordHasher implements PasswordHasherInterface
{
    public function hash(string $password): string
    {
        return password_hash($password, PASSWORD_BCRYPT);
    }

    public function isValid(string $hashedPassword, string $password): bool
    {
        return password_verify($password, $hashedPassword);
    }
}
