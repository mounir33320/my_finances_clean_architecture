<?php

namespace App\Infrastructure\Repository;

use App\Domain\Debt\Entity\Debt;
use App\Domain\Debt\Repository\DebtRepositoryInterface;

class DebtInMemoryRepository implements DebtRepositoryInterface
{
    /**
     * @var Debt[]
     */
    private array $debts = [];

    public function createDebt(Debt $debt): Debt
    {
        $this->debts[] = $debt;

        return $debt;
    }

    public function updateDebt(Debt $debt): Debt
    {
        $filteredDebts = array_filter($this->debts, function (Debt $currentDebt) use ($debt) {
            return $currentDebt->getUuid() === $debt->getUuid();
        });

        $key = array_key_first($filteredDebts);
        $this->debts[$key] = $debt;

        return $debt;
    }

    public function deleteDebt(Debt $debt): bool
    {
        $filteredDebts = array_filter($this->debts, function (Debt $currentDebt) use ($debt) {
            return $currentDebt->getUuid() === $debt->getUuid();
        });

        $key = array_key_first($filteredDebts);
        unset($this->debts[$key]);

        return true;
    }

    public function getDebtByUuidAndUserUuid(string $uuid, string $userUuid): ?Debt
    {
        $filteredDebts = array_filter($this->debts, function (Debt $currentDebt) use ($uuid, $userUuid) {
            return $currentDebt->getUuid() === $uuid && $currentDebt->getUserUuid() === $userUuid;
        });

        if (empty($filteredDebts)) {
            return null;
        }

        return end($filteredDebts);
    }

    /**
     * @return Debt[]
     */
    public function getAllDebts(string $userUuid): array
    {
        return $this->debts;
    }
}
