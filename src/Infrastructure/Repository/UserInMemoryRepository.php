<?php

namespace App\Infrastructure\Repository;

use App\Domain\User\Entity\User;
use App\Domain\User\Repository\UserRepositoryInterface;

class UserInMemoryRepository implements UserRepositoryInterface
{
    /**
     * @var User[]
     */
    private array $users = [];

    public function getUserByUsername(string $username): ?User
    {
        $find = function (User $user) use ($username) {
            return $user->getUsername() === $username;
        };

        $user = array_values(array_filter($this->users, $find));

        if (empty($user)) {
            return null;
        }

        return $user[0];
    }

    public function getUserByUuid(string $uuid): ?User
    {
        $find = function (User $user) use ($uuid) {
            return $user->getUuid() === $uuid;
        };

        $user = array_values(array_filter($this->users, $find));

        if (empty($user)) {
            return null;
        }

        return $user[0];
    }

    public function createUser(User $user): User
    {
        $this->users[] = $user;

        return $user;
    }

    public function updateUser(User $user): User
    {
        for ($i = 0; $i < count($this->users); ++$i) {
            if ($this->users[$i]->getUuid() === $user->getUuid()) {
                $this->users[$i] = $user;
                break;
            }
        }

        return $user;
    }

    public function deleteUser(User $user): bool
    {
        for ($i = 0; $i < count($this->users); ++$i) {
            if ($this->users[$i]->getUuid() === $user->getUuid()) {
                unset($this->users[$i]);

                return true;
            }
        }

        return false;
    }

    public function getAll(): array
    {
        return $this->users;
    }
}
