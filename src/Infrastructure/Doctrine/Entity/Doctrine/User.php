<?php

namespace App\Infrastructure\Doctrine\Entity\Doctrine;

use App\Domain\User\Enum\RoleEnum;
use App\Infrastructure\Doctrine\Repository\Doctrine\UserDoctrineRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: UserDoctrineRepository::class)]
class User
{
    #[ORM\Id]
    #[ORM\GeneratedValue('NONE')]
    #[ORM\Column(length: 255)]
    private string $uuid;

    #[ORM\Column(length: 50)]
    private string $username;

    #[ORM\Column(length: 255)]
    private string $password;

    #[ORM\Column(length: 50)]
    private RoleEnum $role = RoleEnum::RoleUser;

    #[ORM\Column(nullable: true)]
    private ?int $income = null;

    public function getUuid(): string
    {
        return $this->uuid;
    }

    public function setUuid(string $uuid): static
    {
        $this->uuid = $uuid;

        return $this;
    }

    public function getUsername(): string
    {
        return $this->username;
    }

    public function setUsername(string $username): static
    {
        $this->username = $username;

        return $this;
    }

    public function getPassword(): string
    {
        return $this->password;
    }

    public function setPassword(string $password): static
    {
        $this->password = $password;

        return $this;
    }

    public function getRole(): RoleEnum
    {
        return $this->role;
    }

    public function setRole(RoleEnum $role): static
    {
        $this->role = $role;

        return $this;
    }

    public function getIncome(): ?int
    {
        return $this->income;
    }

    public function setIncome(?int $income): static
    {
        $this->income = $income;

        return $this;
    }
}
