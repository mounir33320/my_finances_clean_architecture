<?php

namespace App\Infrastructure\Doctrine\Repository\Doctrine;

use App\Domain\User\Entity\User as DomainUser;
use App\Domain\User\Repository\UserRepositoryInterface;
use App\Infrastructure\Doctrine\Entity\Doctrine\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<User>
 *
 * @method User|null find($id, $lockMode = null, $lockVersion = null)
 * @method User|null findOneBy(array $criteria, array $orderBy = null)
 * @method User[]    findAll()
 * @method User[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserDoctrineRepository extends ServiceEntityRepository implements UserRepositoryInterface
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, User::class);
    }

    public function getUserByUsername(string $username): ?DomainUser
    {
        $user = $this->findOneBy(['username' => $username]);
        if (!$user) {
            return null;
        }

        return new DomainUser(
            $user->getUuid(),
            $user->getUsername(),
            $user->getPassword(),
            $user->getRole(),
            $user->getIncome()
        );
    }

    public function getUserByUuid(string $uuid): ?DomainUser
    {
        $user = $this->findOneBy(['uuid' => $uuid]);
        if (!$user) {
            return null;
        }

        return new DomainUser(
            $user->getUuid(),
            $user->getUsername(),
            $user->getPassword(),
            $user->getRole(),
            $user->getIncome()
        );
    }

    public function createUser(DomainUser $user): DomainUser
    {
        $entityUser = (new User())
            ->setUuid($user->getUuid())
            ->setUsername($user->getUsername())
            ->setPassword($user->getPassword())
            ->setRole($user->getRole())
            ->setIncome($user->getIncome());

        $this->getEntityManager()->persist($entityUser);
        $this->getEntityManager()->flush();

        return $user;
    }

    public function updateUser(DomainUser $user): DomainUser
    {
        $entityUser = $this->findOneBy(['uuid' => $user->getUuid()]);

        if (!$entityUser) {
            throw new \RuntimeException("User with uuid \"{$user->getUuid()}\" not found");
        }

        $entityUser
            ->setUsername($user->getUsername())
            ->setPassword($user->getPassword())
            ->setRole($user->getRole())
            ->setIncome($user->getIncome());

        $this->getEntityManager()->flush();

        return $user;
    }

    public function deleteUser(DomainUser $user): bool
    {
        $entityUser = $this->findOneBy(['uuid' => $user->getUuid()]);

        if (!$entityUser) {
            throw new \RuntimeException("User with uuid \"{$user->getUuid()}\" not found");
        }

        $this->getEntityManager()->remove($entityUser);
        $this->getEntityManager()->flush();

        return true;
    }

    public function getAll(): array
    {
        $domainUsers = [];
        $users = $this->findAll();

        foreach ($users as $user) {
            $domainUsers[] = new DomainUser(
                $user->getUuid(),
                $user->getUsername(),
                $user->getPassword(),
                $user->getRole(),
                $user->getIncome()
            );
        }

        return $domainUsers;
    }
}
