<?php

namespace App\Infrastructure\Symfony\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;

#[Route('/', 'home')]
class HomeController extends AbstractController
{
    public function __invoke(): Response
    {
        return new Response('Home page');
    }
}
