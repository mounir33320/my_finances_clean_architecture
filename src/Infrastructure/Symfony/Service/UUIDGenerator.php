<?php

namespace App\Infrastructure\Symfony\Service;

use App\Application\Service\UUIDGeneratorInterface;
use Symfony\Component\Uid\Uuid;

class UUIDGenerator implements UUIDGeneratorInterface
{
    public function generate(): string
    {
        return Uuid::v4()->toHex();
    }
}
