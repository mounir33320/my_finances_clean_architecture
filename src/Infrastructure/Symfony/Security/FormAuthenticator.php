<?php

namespace App\Infrastructure\Symfony\Security;

use App\Application\User\Login\LoginInterface;
use App\Application\User\Login\LoginPresenterInterface;
use App\Application\User\Login\LoginRequest;
use App\Application\User\Login\LoginResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\UserNotFoundException;
use Symfony\Component\Security\Core\User\PasswordUpgraderInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Http\Authenticator\AbstractLoginFormAuthenticator;
use Symfony\Component\Security\Http\Authenticator\Passport\Badge\CsrfTokenBadge;
use Symfony\Component\Security\Http\Authenticator\Passport\Badge\PasswordUpgradeBadge;
use Symfony\Component\Security\Http\Authenticator\Passport\Badge\RememberMeBadge;
use Symfony\Component\Security\Http\Authenticator\Passport\Badge\UserBadge;
use Symfony\Component\Security\Http\Authenticator\Passport\Credentials\PasswordCredentials;
use Symfony\Component\Security\Http\Authenticator\Passport\Passport;
use Symfony\Component\Security\Http\Util\TargetPathTrait;

class FormAuthenticator extends AbstractLoginFormAuthenticator implements LoginPresenterInterface
{
    use TargetPathTrait;

    private LoginResponse $response;

    public function __construct(
        private readonly UrlGeneratorInterface $urlGenerator,
        private readonly UserProviderInterface $userProvider,
        private readonly LoginInterface $login
    ) {
    }

    public function authenticate(Request $request): Passport
    {
        $csrfToken = is_string($request->get('_csrf_token')) ? $request->get('_csrf_token') : '';
        $username =  is_string($request->get('username')) ? $request->get('username') : '';
        $password = is_string($request->get('password')) ? $request->get('password') : '';

        $this->login->execute(new LoginRequest($username, $password), $this);

        if ($this->response->getNotification()->hasError()) {
            throw new UserNotFoundException($this->response->getNotification()->getErrors()[0]);
        }

        $userBadge = new UserBadge($username, $this->userProvider->loadUserByIdentifier(...));
        $passport = new Passport($userBadge, new PasswordCredentials($password), [new RememberMeBadge()]);
        $passport->addBadge(new CsrfTokenBadge('authenticate', $csrfToken));

        if ($this->userProvider instanceof PasswordUpgraderInterface) {
            $passport->addBadge(new PasswordUpgradeBadge($password, $this->userProvider));
        }

        return $passport;
    }

    public function onAuthenticationSuccess(Request $request, TokenInterface $token, string $firewallName): ?Response
    {
        $targetPath = $this->getTargetPath($request->getSession(), $firewallName);
        $targetPath = $targetPath ?? $this->urlGenerator->generate('home');

        return new RedirectResponse($targetPath);
    }

    protected function getLoginUrl(Request $request): string
    {
        return $this->urlGenerator->generate('login');
    }

    public function present(LoginResponse $response): void
    {
        $this->response = $response;
    }
}
