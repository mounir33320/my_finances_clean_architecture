<?php

namespace App\Infrastructure\Symfony\Security;

use App\Domain\User\Entity\User as DomainUser;
use App\Domain\User\Enum\RoleEnum;
use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;
use Symfony\Component\Security\Core\User\UserInterface;

readonly class User implements UserInterface, PasswordAuthenticatedUserInterface
{
    public function __construct(private DomainUser $user)
    {
    }

    public function getUsername(): ?string
    {
        return $this->user->getUsername();
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUserIdentifier(): string
    {
        return $this->user->getUsername();
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        if (RoleEnum::RoleAdmin === $this->user->getRole()) {
            return ['ROLE_ADMIN'];
        }

        return ['ROLE_USER'];
    }

    /**
     * @see PasswordAuthenticatedUserInterface
     */
    public function getPassword(): string
    {
        return $this->user->getPassword();
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials(): void
    {
    }
}
