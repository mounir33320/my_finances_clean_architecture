<?php

namespace App\Application\Shared\Error;

class Error
{
    public function __construct(private readonly string $fieldName, private readonly string $message)
    {
    }

    public function getFieldName(): string
    {
        return $this->fieldName;
    }

    public function getMessage(): string
    {
        return $this->message;
    }

    public function __toString()
    {
        return "$this->fieldName:$this->message";
    }
}
