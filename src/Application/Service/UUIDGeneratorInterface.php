<?php

namespace App\Application\Service;

interface UUIDGeneratorInterface
{
    public function generate(): string;
}
