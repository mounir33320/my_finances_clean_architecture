<?php

namespace App\Application\Debt\CreateDebt;

interface CreateDebtInterface
{
    public function execute(CreateDebtRequest $request, CreateDebtPresenterInterface $presenter): void;
}
