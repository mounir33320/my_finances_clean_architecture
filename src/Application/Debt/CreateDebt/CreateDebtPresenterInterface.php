<?php

namespace App\Application\Debt\CreateDebt;

interface CreateDebtPresenterInterface
{
    public function present(CreateDebtResponse $response): void;
}
