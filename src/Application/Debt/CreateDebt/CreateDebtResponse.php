<?php

namespace App\Application\Debt\CreateDebt;

use App\Application\Shared\Error\ErrorNotificationInterface;
use App\Domain\Debt\Entity\Debt;

class CreateDebtResponse
{
    private ?Debt $debt = null;

    public function __construct(private readonly ErrorNotificationInterface $errorNotification)
    {
    }

    public function getErrorNotification(): ErrorNotificationInterface
    {
        return $this->errorNotification;
    }

    public function setCreadtedDebt(Debt $debt): void
    {
        $this->debt = $debt;
    }

    public function getCreatedDebt(): ?Debt
    {
        return $this->debt;
    }
}
