<?php

namespace App\Application\Debt\CreateDebt;

readonly class CreateDebtRequest
{
    public function __construct(public string $userUuid, public int $totalAmount, public string $creditorName)
    {
    }
}
