<?php

namespace App\Application\Debt\CreateDebt;

use App\Application\Debt\Shared\DebtErrorMessageConstants;
use App\Application\Service\UUIDGeneratorInterface;
use App\Application\Shared\Error\ErrorNotification;
use App\Application\User\Shared\UserErrorMessageConstants;
use App\Domain\Debt\Entity\Debt;
use App\Domain\Debt\Repository\DebtRepositoryInterface;
use App\Domain\User\Repository\UserRepositoryInterface;
use Assert\Assert;
use Assert\LazyAssertionException;

readonly class CreateDebt implements CreateDebtInterface
{
    public function __construct(
        private UUIDGeneratorInterface $uuidGenerator,
        private DebtRepositoryInterface $debtRepository,
        private UserRepositoryInterface $userRepository
    ) {
    }

    public function execute(CreateDebtRequest $request, CreateDebtPresenterInterface $presenter): void
    {
        $response = new CreateDebtResponse(new ErrorNotification());

        $isValid = $this->checkRequest($request, $response);
        $isValid = $isValid && $this->checkUser($request, $response);
        if ($isValid) {
            $uuid = $this->uuidGenerator->generate();
            $debt = new Debt($uuid, $request->userUuid, $request->totalAmount, $request->creditorName);
            $debt = $this->debtRepository->createDebt($debt);
            $response->setCreadtedDebt($debt);
        }

        $presenter->present($response);
    }

    private function checkRequest(CreateDebtRequest $request, CreateDebtResponse $response): bool
    {
        try {
            Assert::lazy()
                ->that($request->creditorName, 'creditorName')
                    ->notEmpty(DebtErrorMessageConstants::CREDITOR_NAME_CANNOT_BE_EMPTY)
                ->that($request->userUuid, 'userUuid')
                    ->notEmpty(DebtErrorMessageConstants::USER_UUID_CANNOT_BE_EMPTY)
                ->that($request->totalAmount, 'totalAmount')
                    ->min(0, DebtErrorMessageConstants::TOTAL_AMOUNT_CANNOT_BE_NEGATIVE)
                    ->max(1000000, DebtErrorMessageConstants::TOTAL_AMOUNT_CANNOT_BE_GREATER_THAN_1_MILLION)
                ->verifyNow()
            ;

            return true;
        } catch (LazyAssertionException $exception) {
            foreach ($exception->getErrorExceptions() as $errorException) {
                $response->getErrorNotification()->add((string) $errorException->getPropertyPath(), $errorException->getMessage());
            }

            return false;
        }
    }

    private function checkUser(CreateDebtRequest $request, CreateDebtResponse $response): bool
    {
        $user = $this->userRepository->getUserByUuid($request->userUuid);
        if (!$user) {
            $response->getErrorNotification()->add('userUuid', UserErrorMessageConstants::USER_NOT_FOUND);

            return false;
        }

        return true;
    }
}
