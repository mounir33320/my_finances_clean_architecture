<?php

namespace App\Application\Debt\RemoveDebt;

readonly class RemoveDebtRequest
{
    public function __construct(public string $debtUuid, public string $currentUserUuid)
    {
    }
}
