<?php

namespace App\Application\Debt\RemoveDebt;

use App\Application\Debt\Shared\DebtErrorMessageConstants;
use App\Application\Shared\Error\ErrorNotification;
use App\Domain\Debt\Entity\Debt;
use App\Domain\Debt\Repository\DebtRepositoryInterface;

readonly class RemoveDebt implements RemoveDebtInterface
{
    public function __construct(private DebtRepositoryInterface $debtRepository)
    {
    }

    public function execute(RemoveDebtRequest $request, RemoveDebtPresenterInterface $presenter): void
    {
        $response = new RemoveDebtResponse(new ErrorNotification());

        $debt = $this->getDebtOrFalse($request, $response);

        if ($debt) {
            $this->debtRepository->deleteDebt($debt);
        }

        $presenter->present($response);
    }

    private function getDebtOrFalse(RemoveDebtRequest $request, RemoveDebtResponse $response): false|Debt
    {
        $debt = $this->debtRepository->getDebtByUuidAndUserUuid($request->debtUuid, $request->currentUserUuid);

        if (!$debt) {
            $response->getErrorNotification()->add('uuid', DebtErrorMessageConstants::DEBT_NOT_FOUND);

            return false;
        }

        return $debt;
    }
}
