<?php

namespace App\Application\Debt\RemoveDebt;

interface RemoveDebtPresenterInterface
{
    public function present(RemoveDebtResponse $response): void;
}
