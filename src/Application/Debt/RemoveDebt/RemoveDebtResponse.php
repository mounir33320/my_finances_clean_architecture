<?php

namespace App\Application\Debt\RemoveDebt;

use App\Application\Shared\Error\ErrorNotificationInterface;

readonly class RemoveDebtResponse
{
    public function __construct(private ErrorNotificationInterface $errorNotification)
    {
    }

    public function getErrorNotification(): ErrorNotificationInterface
    {
        return $this->errorNotification;
    }
}
