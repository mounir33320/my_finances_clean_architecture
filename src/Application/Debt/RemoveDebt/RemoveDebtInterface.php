<?php

namespace App\Application\Debt\RemoveDebt;

interface RemoveDebtInterface
{
    public function execute(RemoveDebtRequest $request, RemoveDebtPresenterInterface $presenter): void;
}
