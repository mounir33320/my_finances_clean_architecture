<?php

namespace App\Application\Debt\Shared;

class DebtErrorMessageConstants
{
    public const USER_UUID_CANNOT_BE_EMPTY = 'user_uuid_cannot_be_empty';
    public const CREDITOR_NAME_CANNOT_BE_EMPTY = 'creditor_name_cannot_be_empty';
    public const TOTAL_AMOUNT_CANNOT_BE_NEGATIVE = 'total_amount_cannot_be_negative';
    public const TOTAL_AMOUNT_CANNOT_BE_GREATER_THAN_1_MILLION = 'total_amount_cannot_be_greater_than_1_million';
    public const DEBT_NOT_FOUND = 'debt_not_found';
    public const DEBT_UUID_CANNOT_BE_EMPTY = 'debt_uuid_cannod_be_empty';
}
