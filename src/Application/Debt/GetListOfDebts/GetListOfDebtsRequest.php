<?php

namespace App\Application\Debt\GetListOfDebts;

class GetListOfDebtsRequest
{
    public function __construct(public string $currentUserUuid)
    {
    }
}
