<?php

namespace App\Application\Debt\GetListOfDebts;

use App\Application\Shared\Error\ErrorNotificationInterface;
use App\Domain\Debt\Entity\Debt;

class GetListOfDebtsResponse
{
    /**
     * @var Debt[]|array
     */
    private array $listOfDebts = [];

    public function __construct(private readonly ErrorNotificationInterface $errorNotification)
    {
    }

    public function getErrorNotification(): ErrorNotificationInterface
    {
        return $this->errorNotification;
    }

    /**
     * @param Debt[]|array $listOfDebts
     */
    public function setListOfDebts(array $listOfDebts): void
    {
        $this->listOfDebts = $listOfDebts;
    }

    /**
     * @return array|Debt[]
     */
    public function getListOfDebts(): array
    {
        return $this->listOfDebts;
    }
}
