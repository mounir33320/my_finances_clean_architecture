<?php

namespace App\Application\Debt\GetListOfDebts;

use App\Application\Debt\Shared\DebtErrorMessageConstants;
use App\Application\Shared\Error\ErrorNotification;
use App\Domain\Debt\Repository\DebtRepositoryInterface;
use Assert\Assert;
use Assert\LazyAssertionException;

readonly class GetListOfDebts implements GetListOfDebtsInterface
{
    public function __construct(private DebtRepositoryInterface $debtRepository)
    {
    }

    public function execute(GetListOfDebtsRequest $request, GetListOfDebtsPresenterInterface $presenter): void
    {
        $response = new GetListOfDebtsResponse(new ErrorNotification());

        $isValid = $this->checkRequest($request, $response);

        if ($isValid) {
            $listOfDebts = $this->debtRepository->getAllDebts($request->currentUserUuid);
            $response->setListOfDebts($listOfDebts);
        }

        $presenter->present($response);
    }

    private function checkRequest(GetListOfDebtsRequest $request, GetListOfDebtsResponse $response): bool
    {
        try {
            Assert::lazy()
                ->that($request->currentUserUuid, 'user')
                ->notEmpty(DebtErrorMessageConstants::USER_UUID_CANNOT_BE_EMPTY)
                ->verifyNow()
            ;

            return true;
        } catch (LazyAssertionException $exception) {
            foreach ($exception->getErrorExceptions() as $errorException) {
                $response->getErrorNotification()->add((string) $errorException->getPropertyPath(), $errorException->getMessage());
            }

            return false;
        }
    }
}
