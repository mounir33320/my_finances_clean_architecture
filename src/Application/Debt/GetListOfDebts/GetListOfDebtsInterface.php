<?php

namespace App\Application\Debt\GetListOfDebts;

interface GetListOfDebtsInterface
{
    public function execute(GetListOfDebtsRequest $request, GetListOfDebtsPresenterInterface $presenter): void;
}
