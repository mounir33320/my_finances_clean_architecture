<?php

namespace App\Application\Debt\GetListOfDebts;

interface GetListOfDebtsPresenterInterface
{
    public function present(GetListOfDebtsResponse $response): void;
}
