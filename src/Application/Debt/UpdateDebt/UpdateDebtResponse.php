<?php

namespace App\Application\Debt\UpdateDebt;

use App\Application\Shared\Error\ErrorNotificationInterface;
use App\Domain\Debt\Entity\Debt;

class UpdateDebtResponse
{
    private ?Debt $updatedDebt = null;

    public function __construct(private readonly ErrorNotificationInterface $errorNotification)
    {
    }

    public function getErrorNotification(): ErrorNotificationInterface
    {
        return $this->errorNotification;
    }

    public function setUpdatedDebt(Debt $updatedDebt): void
    {
        $this->updatedDebt = $updatedDebt;
    }

    public function getUpdatedDebt(): ?Debt
    {
        return $this->updatedDebt;
    }

    public function addError(string $field, string $message): void
    {
        $this->errorNotification->add($field, $message);
    }
}
