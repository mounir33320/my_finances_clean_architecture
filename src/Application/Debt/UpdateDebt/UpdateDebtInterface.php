<?php

namespace App\Application\Debt\UpdateDebt;

interface UpdateDebtInterface
{
    public function execute(UpdateDebtRequest $request, UpdateDebtPresenterInterface $presenter): void;
}
