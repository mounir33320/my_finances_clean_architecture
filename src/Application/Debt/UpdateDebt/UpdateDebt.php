<?php

namespace App\Application\Debt\UpdateDebt;

use App\Application\Debt\Shared\DebtErrorMessageConstants;
use App\Application\Shared\Error\ErrorNotification;
use App\Application\User\Shared\UserErrorMessageConstants;
use App\Domain\Debt\Entity\Debt;
use App\Domain\Debt\Repository\DebtRepositoryInterface;
use App\Domain\User\Repository\UserRepositoryInterface;
use Assert\Assert;
use Assert\LazyAssertionException;

readonly class UpdateDebt implements UpdateDebtInterface
{
    public function __construct(private DebtRepositoryInterface $debtRepository, private UserRepositoryInterface $userRepository)
    {
    }

    public function execute(UpdateDebtRequest $request, UpdateDebtPresenterInterface $presenter): void
    {
        $response = new UpdateDebtResponse(new ErrorNotification());

        $isValid = $this->checkRequest($request, $response);
        $isValid = $isValid && $this->checkDebt($request, $response);
        $isValid = $isValid && $this->checkCurrentUser($request, $response);

        if ($isValid) {
            $updatedDebt = new Debt($request->debtUuid, $request->currentUserUuid, $request->totalAmount, $request->creditorName);
            $updatedDebt = $this->debtRepository->updateDebt($updatedDebt);

            $response->setUpdatedDebt($updatedDebt);
        }

        $presenter->present($response);
    }

    private function checkCurrentUser(UpdateDebtRequest $request, UpdateDebtResponse $response): bool
    {
        $currentUser = $this->userRepository->getUserByUuid($request->currentUserUuid);

        if (!$currentUser) {
            $response->addError('userUuid', UserErrorMessageConstants::USER_NOT_FOUND);

            return false;
        }

        return true;
    }

    private function checkDebt(UpdateDebtRequest $request, UpdateDebtResponse $response): bool
    {
        $debt = $this->debtRepository->getDebtByUuidAndUserUuid($request->debtUuid, $request->currentUserUuid);

        if (!$debt || $debt->getUserUuid() !== $request->currentUserUuid) {
            $response->addError('uuid', DebtErrorMessageConstants::DEBT_NOT_FOUND);

            return false;
        }

        return true;
    }

    private function checkRequest(UpdateDebtRequest $request, UpdateDebtResponse $response): bool
    {
        try {
            Assert::lazy()
                ->that($request->creditorName, 'creditorName')
                    ->notEmpty(DebtErrorMessageConstants::CREDITOR_NAME_CANNOT_BE_EMPTY)
                ->that($request->debtUuid, 'uuid')
                    ->notEmpty(DebtErrorMessageConstants::DEBT_UUID_CANNOT_BE_EMPTY)
                ->that($request->currentUserUuid, 'userUuid')
                    ->notEmpty(DebtErrorMessageConstants::USER_UUID_CANNOT_BE_EMPTY)
                ->that($request->totalAmount, 'totalAmount')
                    ->min(0, DebtErrorMessageConstants::TOTAL_AMOUNT_CANNOT_BE_NEGATIVE)
                    ->max(1000000, DebtErrorMessageConstants::TOTAL_AMOUNT_CANNOT_BE_GREATER_THAN_1_MILLION)
                ->verifyNow()
            ;

            return true;
        } catch (LazyAssertionException $exception) {
            foreach ($exception->getErrorExceptions() as $errorException) {
                $response->getErrorNotification()->add((string) $errorException->getPropertyPath(), $errorException->getMessage());
            }

            return false;
        }
    }
}
