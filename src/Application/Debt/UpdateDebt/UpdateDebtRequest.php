<?php

namespace App\Application\Debt\UpdateDebt;

readonly class UpdateDebtRequest
{
    public function __construct(public string $currentUserUuid, public string $debtUuid, public int $totalAmount, public string $creditorName)
    {
    }
}
