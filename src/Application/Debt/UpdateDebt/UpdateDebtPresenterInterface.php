<?php

namespace App\Application\Debt\UpdateDebt;

interface UpdateDebtPresenterInterface
{
    public function present(UpdateDebtResponse $response): void;
}
