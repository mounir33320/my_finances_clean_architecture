<?php

namespace App\Application\User\DeleteUser;

readonly class DeleteUserRequest
{
    public function __construct(public string $currentUserUuid, public string $userToDeleteUuid)
    {
    }
}
