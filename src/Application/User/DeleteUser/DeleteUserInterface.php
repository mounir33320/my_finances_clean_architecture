<?php

namespace App\Application\User\DeleteUser;

interface DeleteUserInterface
{
    public function execute(DeleteUserRequest $request, DeleteUserPresenterInterface $presenter): void;
}
