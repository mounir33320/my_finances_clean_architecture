<?php

namespace App\Application\User\DeleteUser;

interface DeleteUserPresenterInterface
{
    public function present(DeleteUserResponse $response): void;
}
