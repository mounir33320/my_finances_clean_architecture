<?php

namespace App\Application\User\DeleteUser;

use App\Application\Shared\Error\ErrorNotificationInterface;

readonly class DeleteUserResponse
{
    public function __construct(private ErrorNotificationInterface $errorNotification)
    {
    }

    public function getErrorNotification(): ErrorNotificationInterface
    {
        return $this->errorNotification;
    }
}
