<?php

namespace App\Application\User\DeleteUser;

use App\Application\Shared\Error\ErrorNotificationInterface;
use App\Application\User\Shared\UserErrorMessageConstants;
use App\Domain\User\Entity\User;
use App\Domain\User\Enum\RoleEnum;
use App\Domain\User\Repository\UserRepositoryInterface;

readonly class DeleteUser implements DeleteUserInterface
{
    public function __construct(private UserRepositoryInterface $userRepository, private ErrorNotificationInterface $errorNotification)
    {
    }

    public function execute(DeleteUserRequest $request, DeleteUserPresenterInterface $presenter): void
    {
        $response = new DeleteUserResponse($this->errorNotification);

        $currentUser = $this->userRepository->getUserByUuid($request->currentUserUuid);
        $userToDelete = $this->userRepository->getUserByUuid($request->userToDeleteUuid);

        $isValid = $this->checkUser($currentUser, $response);
        $isValid = $isValid && $this->checkUser($userToDelete, $response);
        $isValid = $isValid && $currentUser && $userToDelete && $this->canDelete($currentUser, $userToDelete, $response);

        if ($isValid) {
            $this->userRepository->deleteUser($userToDelete);
        }

        $presenter->present($response);
    }

    private function canDelete(User $currentUser, User $userToDelete, DeleteUserResponse $response): bool
    {
        if ($currentUser->getUuid() === $userToDelete->getUuid()) {
            return true;
        }

        if (RoleEnum::RoleAdmin === $userToDelete->getRole() && RoleEnum::RoleAdmin === $currentUser->getRole()) {
            $response->getErrorNotification()->add('role', UserErrorMessageConstants::ADMIN_USER_CANNOT_BE_DELETED_BY_AN_ANOTHER_USER);

            return false;
        }

        if (RoleEnum::RoleAdmin === $currentUser->getRole()) {
            return true;
        }

        $response->getErrorNotification()->add('role', UserErrorMessageConstants::CURRENT_USER_MUST_BE_ADMIN);

        return false;
    }

    private function checkUser(?User $currentUser, DeleteUserResponse $response): bool
    {
        if (!$currentUser instanceof User) {
            $response->getErrorNotification()->add('uuid', UserErrorMessageConstants::USER_NOT_FOUND);

            return false;
        }

        return true;
    }
}
