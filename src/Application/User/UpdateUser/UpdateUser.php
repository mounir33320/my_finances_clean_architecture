<?php

namespace App\Application\User\UpdateUser;

use App\Application\Shared\Error\ErrorNotificationInterface;
use App\Application\User\Shared\UserErrorMessageConstants;
use App\Domain\User\Entity\User;
use App\Domain\User\Enum\RoleEnum;
use App\Domain\User\Repository\UserRepositoryInterface;
use Assert\Assert;
use Assert\LazyAssertionException;

readonly class UpdateUser implements UpdateUserInterface
{
    public function __construct(private UserRepositoryInterface $userRepository, private ErrorNotificationInterface $notification)
    {
    }

    public function execute(UpdateUserRequest $request, UpdateUserPresenterInterface $presenter): void
    {
        $response = new UpdateUserResponse($this->notification);

        $isValid = $this->checkRequest($request, $response);
        $isValid = $isValid && $this->checkRole($request, $response);
        $isValid = $isValid && $this->checkCurrentUser($request, $response);
        $isValid = $isValid && $this->checkUserToUpdate($request, $response);

        if ($isValid) {
            $updatedUser = new User(
                $request->uuid,
                $request->username,
                $response->getOriginalUser()->getPassword(),
                RoleEnum::from($request->role),
                $request->income
            );

            $updatedUser = $this->userRepository->updateUser($updatedUser);
            $response->setUpdatedUser($updatedUser);
        }

        $presenter->present($response);
    }

    private function checkRequest(UpdateUserRequest $request, UpdateUserResponse $response): bool
    {
        try {
            Assert::lazy()
                ->that($request->username, 'username')
                    ->minLength(2, UserErrorMessageConstants::USERNAME_LENGTH_MUST_BE_GREATER_THAN_1)
                ->that($request->income, 'income')
                    ->nullOr()
                    ->min(0, UserErrorMessageConstants::INCOME_CANNOT_BE_NEGATIVE)
                ->verifyNow();

            return true;
        } catch (LazyAssertionException $exception) {
            foreach ($exception->getErrorExceptions() as $errorException) {
                $response->getErrorNotification()->add((string) $errorException->getPropertyPath(), $errorException->getMessage());
            }

            return false;
        }
    }

    private function checkRole(UpdateUserRequest $request, UpdateUserResponse $response): bool
    {
        if (!RoleEnum::tryFrom($request->role)) {
            $response->getErrorNotification()->add('role', UserErrorMessageConstants::INVALID_ROLE);

            return false;
        }

        return true;
    }

    private function checkCurrentUser(UpdateUserRequest $request, UpdateUserResponse $response): bool
    {
        $currentUser = $this->userRepository->getUserByUuid($request->currentUserUuid);

        if (!$currentUser) {
            $response->getErrorNotification()->add('user', UserErrorMessageConstants::USER_NOT_FOUND);

            return false;
        }

        if ($currentUser->getUuid() !== $request->uuid && RoleEnum::RoleAdmin !== $currentUser->getRole()) {
            $response->getErrorNotification()->add('role', UserErrorMessageConstants::CURRENT_USER_MUST_BE_ADMIN);

            return false;
        }

        return true;
    }

    private function checkUserToUpdate(UpdateUserRequest $request, UpdateUserResponse $response): bool
    {
        $userToUpdate = $this->userRepository->getUserByUuid($request->uuid);

        if (!$userToUpdate) {
            $response->getErrorNotification()->add('user', UserErrorMessageConstants::USER_NOT_FOUND);

            return false;
        }

        if (RoleEnum::RoleAdmin === $userToUpdate->getRole()) {
            $response->getErrorNotification()->add('role', UserErrorMessageConstants::ADMIN_USER_CANNOT_BE_UPDATED_BY_AN_ANOTHER_USER);
        }

        $response->setOriginalUser($userToUpdate);

        return true;
    }
}
