<?php

namespace App\Application\User\UpdateUser;

readonly class UpdateUserRequest
{
    public function __construct(public string $currentUserUuid, public string $uuid, public string $username, public string $role, public ?int $income = null)
    {
    }
}
