<?php

namespace App\Application\User\UpdateUser;

interface UpdateUserPresenterInterface
{
    public function present(UpdateUserResponse $response): void;
}
