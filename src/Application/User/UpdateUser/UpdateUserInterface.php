<?php

namespace App\Application\User\UpdateUser;

interface UpdateUserInterface
{
    public function execute(UpdateUserRequest $request, UpdateUserPresenterInterface $presenter): void;
}
