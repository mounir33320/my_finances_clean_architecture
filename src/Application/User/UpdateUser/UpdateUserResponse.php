<?php

namespace App\Application\User\UpdateUser;

use App\Application\Shared\Error\ErrorNotificationInterface;
use App\Domain\User\Entity\User;

class UpdateUserResponse
{
    private User $originalUser;
    private User $updatedUser;

    public function __construct(private readonly ErrorNotificationInterface $errorNotification)
    {
    }

    public function getErrorNotification(): ErrorNotificationInterface
    {
        return $this->errorNotification;
    }

    public function getOriginalUser(): User
    {
        return $this->originalUser;
    }

    public function setOriginalUser(User $userToUpdate): void
    {
        $this->originalUser = $userToUpdate;
    }

    public function getUpdatedUser(): User
    {
        return $this->updatedUser;
    }

    public function setUpdatedUser(User $updatedUser): void
    {
        $this->updatedUser = $updatedUser;
    }
}
