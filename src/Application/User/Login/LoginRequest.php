<?php

namespace App\Application\User\Login;

class LoginRequest
{
    public function __construct(public string $username, public string $password)
    {
    }
}
