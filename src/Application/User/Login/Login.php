<?php

namespace App\Application\User\Login;

use App\Application\Shared\Error\ErrorNotificationInterface;
use App\Application\User\Service\PasswordHasherInterface;
use App\Application\User\Shared\UserErrorMessageConstants;
use App\Domain\User\Entity\User;
use App\Domain\User\Repository\UserRepositoryInterface;

readonly class Login implements LoginInterface
{
    public function __construct(
        private ErrorNotificationInterface $notification,
        private UserRepositoryInterface $userRepository,
        private PasswordHasherInterface $passwordHasher
    ) {
    }

    public function execute(LoginRequest $request, LoginPresenterInterface $presenter): void
    {
        $response = new LoginResponse($this->notification);
        $user = $this->userRepository->getUserByUsername($request->username);

        $isValid = $this->checkUser($user, $response);
        $isValid = $isValid && $user && $this->checkPassword($request, $user, $response);

        if ($isValid) {
            $response->setUser($user);
        }

        $presenter->present($response);
    }

    private function checkUser(?User $user, LoginResponse $response): bool
    {
        if (null === $user) {
            $response->getNotification()->add('username', UserErrorMessageConstants::UNKNOWN_USERNAME);

            return false;
        }

        return true;
    }

    private function checkPassword(LoginRequest $request, User $user, LoginResponse $response): bool
    {
        if (!$this->passwordHasher->isValid($user->getPassword(), $request->password)) {
            $response->getNotification()->add('password', UserErrorMessageConstants::INVALID_CREDENTIALS);

            return false;
        }

        return true;
    }
}
