<?php

namespace App\Application\User\Login;

interface LoginPresenterInterface
{
    public function present(LoginResponse $response): void;
}
