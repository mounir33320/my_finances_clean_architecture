<?php

namespace App\Application\User\Login;

use App\Application\Shared\Error\ErrorNotificationInterface;
use App\Domain\User\Entity\User;

class LoginResponse
{
    private User $user;

    public function __construct(private readonly ErrorNotificationInterface $notification)
    {
    }

    public function getNotification(): ErrorNotificationInterface
    {
        return $this->notification;
    }

    public function getUser(): User
    {
        return $this->user;
    }

    public function setUser(User $user): LoginResponse
    {
        $this->user = $user;

        return $this;
    }
}
