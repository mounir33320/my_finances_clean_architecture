<?php

namespace App\Application\User\Login;

interface LoginInterface
{
    public function execute(LoginRequest $request, LoginPresenterInterface $presenter): void;
}
