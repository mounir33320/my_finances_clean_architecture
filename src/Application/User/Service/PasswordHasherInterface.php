<?php

namespace App\Application\User\Service;

interface PasswordHasherInterface
{
    public function hash(string $password): string;

    public function isValid(string $hashedPassword, string $password): bool;
}
