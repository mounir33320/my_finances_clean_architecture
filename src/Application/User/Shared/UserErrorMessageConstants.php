<?php

namespace App\Application\User\Shared;

class UserErrorMessageConstants
{
    public const USER_ALREADY_EXISTS = 'user_already_exists';
    public const INVALID_ROLE = 'invalid_role';
    public const USER_NOT_FOUND = 'user_not_found';
    public const CURRENT_USER_MUST_BE_ADMIN = 'current_user_must_be_admin';
    public const USERNAME_LENGTH_MUST_BE_GREATER_THAN_1 = 'username_length_must_be_greater_than_1';
    public const PASSWORD_CANNOT_BE_EMPTY = 'password_cannot_be_empty';
    public const PASSWORD_MUST_CONTAINS_LOWERCASE_LETTER_UPPERCASE_LETTER_NUMBER_SPECIAL_CHARACTER = 'password_must_contains_lowercase_letter_uppercase_letter_number_special_character';
    public const UNKNOWN_USERNAME = 'unknown_username';
    public const INVALID_CREDENTIALS = 'invalid_credentials';
    public const INCOME_CANNOT_BE_NEGATIVE = 'income_cannot_be_negative';
    public const ADMIN_USER_CANNOT_BE_UPDATED_BY_AN_ANOTHER_USER = 'admin_user_cannot_be_updated_by_an_another_user';
    public const ADMIN_USER_CANNOT_BE_DELETED_BY_AN_ANOTHER_USER = 'admin_user_cannot_be_deleted_by_an_another_user';
}
