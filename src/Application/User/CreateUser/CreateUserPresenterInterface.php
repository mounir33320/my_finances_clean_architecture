<?php

namespace App\Application\User\CreateUser;

interface CreateUserPresenterInterface
{
    public function present(CreateUserResponse $response): void;
}
