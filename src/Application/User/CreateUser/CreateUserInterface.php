<?php

namespace App\Application\User\CreateUser;

interface CreateUserInterface
{
    public function execute(CreateUserRequest $request, CreateUserPresenterInterface $presenter): void;
}
