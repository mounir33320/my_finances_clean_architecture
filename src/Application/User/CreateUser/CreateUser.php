<?php

namespace App\Application\User\CreateUser;

use App\Application\Service\UUIDGeneratorInterface;
use App\Application\Shared\Error\ErrorNotificationInterface;
use App\Application\User\Service\PasswordHasherInterface;
use App\Application\User\Shared\UserErrorMessageConstants;
use App\Domain\User\Entity\User;
use App\Domain\User\Enum\RoleEnum;
use App\Domain\User\Repository\UserRepositoryInterface;
use Assert\Assert;
use Assert\LazyAssertionException;

readonly class CreateUser implements CreateUserInterface
{
    public function __construct(
        private ErrorNotificationInterface $errorNotification,
        private UserRepositoryInterface $userRepository,
        private PasswordHasherInterface $passwordHasher,
        private UUIDGeneratorInterface $uuidGenerator
    ) {
    }

    public function execute(CreateUserRequest $request, CreateUserPresenterInterface $presenter): void
    {
        $response = new CreateUserResponse($this->errorNotification);

        $isValid = $this->checkCurrentUser($request, $response);
        $isValid = $isValid && $this->checkRequest($request, $response);
        $isValid = $isValid && $this->checkExistingUser($request, $response);
        $isValid = $isValid && $this->checkRole($request, $response);

        if ($isValid) {
            $uuid = $this->uuidGenerator->generate();
            $hashedPassword = $this->passwordHasher->hash($request->password);
            $role = RoleEnum::from($request->role);
            $user = new User($uuid, $request->username, $hashedPassword, $role, $request->income);
            $createdUser = $this->userRepository->createUser($user);

            $response->setCreatedUser($createdUser);
        }

        $presenter->present($response);
    }

    private function checkRequest(CreateUserRequest $request, CreateUserResponse $response): bool
    {
        try {
            $passwordRegex = '/^(?=.*[A-Z])(?=.*[a-z])(?=.*[0-9])(?=.*[:!;,?*$@£&\-_\/()[\]{}#|])[A-Za-z0-9:!;,?*$@£&\-_\/()[\]{}#|]{6,}$/';

            Assert::lazy()
                ->that($request->username, 'username')
                    ->minLength(1, UserErrorMessageConstants::USERNAME_LENGTH_MUST_BE_GREATER_THAN_1)
                ->that($request->password, 'password')
                    ->notEmpty(UserErrorMessageConstants::PASSWORD_CANNOT_BE_EMPTY)
                    ->regex($passwordRegex, UserErrorMessageConstants::PASSWORD_MUST_CONTAINS_LOWERCASE_LETTER_UPPERCASE_LETTER_NUMBER_SPECIAL_CHARACTER)
                ->that($request->income, 'income')
                    ->nullOr()->min(0, UserErrorMessageConstants::INCOME_CANNOT_BE_NEGATIVE)
                ->verifyNow()
            ;

            return true;
        } catch (LazyAssertionException $exception) {
            foreach ($exception->getErrorExceptions() as $errorException) {
                $response->addError((string) $errorException->getPropertyPath(), $errorException->getMessage());
            }

            return false;
        }
    }

    private function checkExistingUser(CreateUserRequest $request, CreateUserResponse $response): bool
    {
        $existingUser = $this->userRepository->getUserByUsername($request->username);

        if ($existingUser) {
            $response->addError('username', UserErrorMessageConstants::USER_ALREADY_EXISTS);

            return false;
        }

        return true;
    }

    private function checkRole(CreateUserRequest $request, CreateUserResponse $response): bool
    {
        if (!RoleEnum::tryFrom($request->role)) {
            $response->addError('role', UserErrorMessageConstants::INVALID_ROLE);

            return false;
        }

        return true;
    }

    private function checkCurrentUser(CreateUserRequest $request, CreateUserResponse $response): bool
    {
        $currentUser = $this->userRepository->getUserByUuid($request->currentUserUuid);

        if (!$currentUser) {
            $response->addError('uuid', UserErrorMessageConstants::USER_NOT_FOUND);

            return false;
        }

        if (RoleEnum::RoleAdmin !== $currentUser->getRole()) {
            $response->addError('role', UserErrorMessageConstants::CURRENT_USER_MUST_BE_ADMIN);

            return false;
        }

        return true;
    }
}
