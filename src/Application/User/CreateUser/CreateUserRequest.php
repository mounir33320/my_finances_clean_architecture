<?php

namespace App\Application\User\CreateUser;

readonly class CreateUserRequest
{
    public function __construct(public string $currentUserUuid, public string $username, public string $password, public string $role, public ?int $income = null)
    {
    }
}
