<?php

namespace App\Domain\User\Enum;

enum RoleEnum: string
{
    case RoleAdmin = 'admin';
    case RoleUser = 'user';
}
