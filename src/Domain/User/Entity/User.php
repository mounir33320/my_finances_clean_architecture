<?php

namespace App\Domain\User\Entity;

use App\Domain\User\Enum\RoleEnum;

class User
{
    public function __construct(
        private string $uuid,
        private string $username,
        private string $password,
        private RoleEnum $role,
        private ?int $income = null
    ) {
    }

    public function getUuid(): string
    {
        return $this->uuid;
    }

    public function setUuid(string $uuid): self
    {
        $this->uuid = $uuid;

        return $this;
    }

    public function getUsername(): string
    {
        return $this->username;
    }

    public function setUsername(string $username): self
    {
        $this->username = $username;

        return $this;
    }

    public function getPassword(): string
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    public function getRole(): RoleEnum
    {
        return $this->role;
    }

    public function setRole(RoleEnum $role): self
    {
        $this->role = $role;

        return $this;
    }

    public function getIncome(): ?int
    {
        return $this->income;
    }

    public function setIncome(?int $income): self
    {
        $this->income = $income;

        return $this;
    }
}
