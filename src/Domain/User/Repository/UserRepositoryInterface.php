<?php

namespace App\Domain\User\Repository;

use App\Domain\User\Entity\User;

interface UserRepositoryInterface
{
    /**
     * @return User[]|array
     */
    public function getAll(): array;

    public function getUserByUsername(string $username): ?User;

    public function getUserByUuid(string $uuid): ?User;

    public function createUser(User $user): User;

    public function updateUser(User $user): User;

    public function deleteUser(User $user): bool;
}
