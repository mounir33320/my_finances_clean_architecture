<?php

namespace App\Domain\Debt\Repository;

use App\Domain\Debt\Entity\Debt;

interface DebtRepositoryInterface
{
    public function createDebt(Debt $debt): Debt;

    public function updateDebt(Debt $debt): Debt;

    public function deleteDebt(Debt $debt): bool;

    public function getDebtByUuidAndUserUuid(string $uuid, string $userUuid): ?Debt;

    /**
     * @return Debt[]
     */
    public function getAllDebts(string $userUuid): array;
}
