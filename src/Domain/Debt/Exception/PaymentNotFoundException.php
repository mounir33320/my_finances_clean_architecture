<?php

namespace App\Domain\Debt\Exception;

class PaymentNotFoundException extends \Exception
{
    public function __construct(string $uuid, int $code = 0, ?\Throwable $previous = null)
    {
        parent::__construct("Payment with UUID \"$uuid\" not found", $code, $previous);
    }
}
