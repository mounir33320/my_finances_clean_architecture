<?php

namespace App\Domain\Debt\Entity;

use App\Domain\Debt\Exception\PaymentNotFoundException;

class Debt
{
    /**
     * @var Payment[]
     */
    private array $payments = [];

    public function __construct(
        private readonly string $uuid,
        private readonly string $userUuid,
        private readonly int $totalAmount,
        private readonly string $creditorName,
    ) {
    }

    public function addPayment(string $uuid, int $amount, ?\DateTimeInterface $paymentDate = null): void
    {
        $paymentDate = $paymentDate ?? new \DateTimeImmutable();
        $this->payments[$uuid] = new Payment($uuid, $this->uuid, $amount, $paymentDate);
    }

    /**
     * @throws PaymentNotFoundException
     */
    public function updatePayment(string $uuid, int $amount, ?\DateTimeInterface $paymentDate = null): void
    {
        if (!array_key_exists($uuid, $this->payments)) {
            throw new PaymentNotFoundException($uuid);
        }

        $paymentDate = $paymentDate ?? new \DateTimeImmutable();
        $this->payments[$uuid] = new Payment($uuid, $this->uuid, $amount, $paymentDate);
    }

    /**
     * @throws PaymentNotFoundException
     */
    public function removePayment(string $uuid): void
    {
        if (!array_key_exists($uuid, $this->payments)) {
            throw new PaymentNotFoundException($uuid);
        }

        unset($this->payments[$uuid]);
    }

    /**
     * @return Payment[]
     */
    public function getPayments(): array
    {
        return $this->payments;
    }

    public function getRemainingAmount(): int
    {
        return $this->totalAmount - $this->getPaidAmount();
    }

    public function getRemainingAmountPercentage(): int
    {
        return (int) round($this->getRemainingAmount() / $this->totalAmount * 100);
    }

    public function getPaidAmount(): int
    {
        return array_reduce($this->payments, function ($carry, $payment) {
            return $carry + $payment->getAmount();
        }, 0);
    }

    public function getPaidAmountPercentage(): int
    {
        return (int) round($this->getPaidAmount() / $this->totalAmount * 100);
    }

    public function getUuid(): string
    {
        return $this->uuid;
    }

    public function getUserUuid(): string
    {
        return $this->userUuid;
    }

    public function getTotalAmount(): int
    {
        return $this->totalAmount;
    }

    public function getCreditorName(): string
    {
        return $this->creditorName;
    }
}
