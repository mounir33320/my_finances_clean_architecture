<?php

namespace App\Domain\Debt\Entity;

readonly class Payment
{
    public function __construct(
        private string $uuid,
        private string $debtUuid,
        private int $amount,
        private \DateTimeInterface $paymentDate
    ) {
    }

    public function getAmount(): int
    {
        return $this->amount;
    }

    public function getPaymentDate(): \DateTimeInterface
    {
        return $this->paymentDate;
    }

    public function getUuid(): string
    {
        return $this->uuid;
    }

    public function getDebtUuid(): string
    {
        return $this->debtUuid;
    }
}
